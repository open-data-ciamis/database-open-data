--
-- PostgreSQL database dump
--

-- Dumped from database version 10.16 (Ubuntu 10.16-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.16 (Ubuntu 10.16-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: nested; Type: TYPE; Schema: public; Owner: ckan_default
--

CREATE TYPE public.nested AS (
	json json,
	extra text
);


ALTER TYPE public.nested OWNER TO ckan_default;

--
-- Name: populate_full_text_trigger(); Type: FUNCTION; Schema: public; Owner: ckan_default
--

CREATE FUNCTION public.populate_full_text_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF NEW._full_text IS NOT NULL THEN
            RETURN NEW;
        END IF;
        NEW._full_text := (
            SELECT to_tsvector(string_agg(value, ' '))
            FROM json_each_text(row_to_json(NEW.*))
            WHERE key NOT LIKE '\_%');
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.populate_full_text_trigger() OWNER TO ckan_default;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: 012f05fe-7c10-4e53-85f7-5022636e571b; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."012f05fe-7c10-4e53-85f7-5022636e571b" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Kelurahan/Desa" text,
    "Latitude" text,
    "Longitude" text,
    "Ketinggian" text
);


ALTER TABLE public."012f05fe-7c10-4e53-85f7-5022636e571b" OWNER TO ckan_default;

--
-- Name: 012f05fe-7c10-4e53-85f7-5022636e571b__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."012f05fe-7c10-4e53-85f7-5022636e571b__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."012f05fe-7c10-4e53-85f7-5022636e571b__id_seq" OWNER TO ckan_default;

--
-- Name: 012f05fe-7c10-4e53-85f7-5022636e571b__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."012f05fe-7c10-4e53-85f7-5022636e571b__id_seq" OWNED BY public."012f05fe-7c10-4e53-85f7-5022636e571b"._id;


--
-- Name: 0e2dd585-af1f-4098-8dbb-bc3916d3902c; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" (
    _id integer NOT NULL,
    _full_text tsvector,
    "APBD TAHUN" numeric,
    "APBD" text,
    "JUMLAH" text,
    "REALISASI" text
);


ALTER TABLE public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" OWNER TO ckan_default;

--
-- Name: 0e2dd585-af1f-4098-8dbb-bc3916d3902c__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."0e2dd585-af1f-4098-8dbb-bc3916d3902c__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."0e2dd585-af1f-4098-8dbb-bc3916d3902c__id_seq" OWNER TO ckan_default;

--
-- Name: 0e2dd585-af1f-4098-8dbb-bc3916d3902c__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."0e2dd585-af1f-4098-8dbb-bc3916d3902c__id_seq" OWNED BY public."0e2dd585-af1f-4098-8dbb-bc3916d3902c"._id;


--
-- Name: 0f6b5cd2-14f9-424c-b1c2-b55e229789e9; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" (
    _id integer NOT NULL,
    _full_text tsvector,
    "No" text,
    "Instansi" text,
    "Islam" numeric,
    "Protestan" numeric,
    "Katolik" numeric,
    "Hindu" text,
    "Budha" numeric,
    "Konghucu" text,
    "Jumlah Total" numeric
);


ALTER TABLE public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" OWNER TO ckan_default;

--
-- Name: 0f6b5cd2-14f9-424c-b1c2-b55e229789e9__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9__id_seq" OWNER TO ckan_default;

--
-- Name: 0f6b5cd2-14f9-424c-b1c2-b55e229789e9__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9__id_seq" OWNED BY public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9"._id;


--
-- Name: 2592a627-a297-4997-89fb-203c76f83354; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."2592a627-a297-4997-89fb-203c76f83354" (
    _id integer NOT NULL,
    _full_text tsvector,
    tahun numeric,
    akses_air_bersih numeric,
    akses_jamban numeric
);


ALTER TABLE public."2592a627-a297-4997-89fb-203c76f83354" OWNER TO ckan_default;

--
-- Name: 2592a627-a297-4997-89fb-203c76f83354__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."2592a627-a297-4997-89fb-203c76f83354__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."2592a627-a297-4997-89fb-203c76f83354__id_seq" OWNER TO ckan_default;

--
-- Name: 2592a627-a297-4997-89fb-203c76f83354__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."2592a627-a297-4997-89fb-203c76f83354__id_seq" OWNED BY public."2592a627-a297-4997-89fb-203c76f83354"._id;


--
-- Name: 284b5526-82fa-4213-bdbe-92e0d6909910; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."284b5526-82fa-4213-bdbe-92e0d6909910" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Judul" text,
    "OPD" text,
    "Jumlah" numeric
);


ALTER TABLE public."284b5526-82fa-4213-bdbe-92e0d6909910" OWNER TO ckan_default;

--
-- Name: 284b5526-82fa-4213-bdbe-92e0d6909910__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."284b5526-82fa-4213-bdbe-92e0d6909910__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."284b5526-82fa-4213-bdbe-92e0d6909910__id_seq" OWNER TO ckan_default;

--
-- Name: 284b5526-82fa-4213-bdbe-92e0d6909910__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."284b5526-82fa-4213-bdbe-92e0d6909910__id_seq" OWNED BY public."284b5526-82fa-4213-bdbe-92e0d6909910"._id;


--
-- Name: 2a59fc35-d9ce-4543-94b1-921e30af80e8; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."2a59fc35-d9ce-4543-94b1-921e30af80e8" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Tahun" numeric,
    "Harapan Hidup (%)" text
);


ALTER TABLE public."2a59fc35-d9ce-4543-94b1-921e30af80e8" OWNER TO ckan_default;

--
-- Name: 2a59fc35-d9ce-4543-94b1-921e30af80e8__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."2a59fc35-d9ce-4543-94b1-921e30af80e8__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."2a59fc35-d9ce-4543-94b1-921e30af80e8__id_seq" OWNER TO ckan_default;

--
-- Name: 2a59fc35-d9ce-4543-94b1-921e30af80e8__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."2a59fc35-d9ce-4543-94b1-921e30af80e8__id_seq" OWNED BY public."2a59fc35-d9ce-4543-94b1-921e30af80e8"._id;


--
-- Name: 34b7eed5-191f-4459-9276-e0479b71b056; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."34b7eed5-191f-4459-9276-e0479b71b056" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Judul" text,
    "OPD" text,
    "Jumlah" numeric
);


ALTER TABLE public."34b7eed5-191f-4459-9276-e0479b71b056" OWNER TO ckan_default;

--
-- Name: 34b7eed5-191f-4459-9276-e0479b71b056__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."34b7eed5-191f-4459-9276-e0479b71b056__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."34b7eed5-191f-4459-9276-e0479b71b056__id_seq" OWNER TO ckan_default;

--
-- Name: 34b7eed5-191f-4459-9276-e0479b71b056__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."34b7eed5-191f-4459-9276-e0479b71b056__id_seq" OWNED BY public."34b7eed5-191f-4459-9276-e0479b71b056"._id;


--
-- Name: 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59" (
    _id integer NOT NULL,
    _full_text tsvector,
    "No" numeric,
    "Nilai Eksport Berdasarkan Negara Tujuan" text,
    "Jumlah (Rp)" numeric
);


ALTER TABLE public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59" OWNER TO ckan_default;

--
-- Name: 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59__id_seq" OWNER TO ckan_default;

--
-- Name: 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59__id_seq" OWNED BY public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59"._id;


--
-- Name: 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea" (
    _id integer NOT NULL,
    _full_text tsvector,
    "No" numeric,
    "Jenis Investasi" text,
    "Nilai" numeric
);


ALTER TABLE public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea" OWNER TO ckan_default;

--
-- Name: 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea__id_seq" OWNER TO ckan_default;

--
-- Name: 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea__id_seq" OWNED BY public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea"._id;


--
-- Name: 5238a3cd-a295-4ba1-8c93-66632195f349; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."5238a3cd-a295-4ba1-8c93-66632195f349" (
    _id integer NOT NULL,
    _full_text tsvector,
    "No" numeric,
    "Indikator" text,
    "2017" numeric,
    "2018" numeric,
    "2019" numeric,
    "2020" numeric
);


ALTER TABLE public."5238a3cd-a295-4ba1-8c93-66632195f349" OWNER TO ckan_default;

--
-- Name: 5238a3cd-a295-4ba1-8c93-66632195f349__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."5238a3cd-a295-4ba1-8c93-66632195f349__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."5238a3cd-a295-4ba1-8c93-66632195f349__id_seq" OWNER TO ckan_default;

--
-- Name: 5238a3cd-a295-4ba1-8c93-66632195f349__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."5238a3cd-a295-4ba1-8c93-66632195f349__id_seq" OWNED BY public."5238a3cd-a295-4ba1-8c93-66632195f349"._id;


--
-- Name: 6098fa82-136c-4b35-aa94-829626f5f98e; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."6098fa82-136c-4b35-aa94-829626f5f98e" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Tahun" numeric,
    "Laju Pertumbuhan Penduduk (%)" text
);


ALTER TABLE public."6098fa82-136c-4b35-aa94-829626f5f98e" OWNER TO ckan_default;

--
-- Name: 6098fa82-136c-4b35-aa94-829626f5f98e__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."6098fa82-136c-4b35-aa94-829626f5f98e__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."6098fa82-136c-4b35-aa94-829626f5f98e__id_seq" OWNER TO ckan_default;

--
-- Name: 6098fa82-136c-4b35-aa94-829626f5f98e__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."6098fa82-136c-4b35-aa94-829626f5f98e__id_seq" OWNED BY public."6098fa82-136c-4b35-aa94-829626f5f98e"._id;


--
-- Name: 677400e2-f613-437d-9957-7dbd2ebb8668; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."677400e2-f613-437d-9957-7dbd2ebb8668" (
    _id integer NOT NULL,
    _full_text tsvector,
    "No." text,
    "Capaian" text,
    "2017" numeric,
    "2018" numeric,
    "2019" numeric,
    "Satuan" text
);


ALTER TABLE public."677400e2-f613-437d-9957-7dbd2ebb8668" OWNER TO ckan_default;

--
-- Name: 677400e2-f613-437d-9957-7dbd2ebb8668__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."677400e2-f613-437d-9957-7dbd2ebb8668__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."677400e2-f613-437d-9957-7dbd2ebb8668__id_seq" OWNER TO ckan_default;

--
-- Name: 677400e2-f613-437d-9957-7dbd2ebb8668__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."677400e2-f613-437d-9957-7dbd2ebb8668__id_seq" OWNED BY public."677400e2-f613-437d-9957-7dbd2ebb8668"._id;


--
-- Name: 6a63f1b2-c070-4d70-99dd-d9199fb15962; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."6a63f1b2-c070-4d70-99dd-d9199fb15962" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Judul" text,
    "OPD" text,
    "Jumlah" numeric
);


ALTER TABLE public."6a63f1b2-c070-4d70-99dd-d9199fb15962" OWNER TO ckan_default;

--
-- Name: 6a63f1b2-c070-4d70-99dd-d9199fb15962__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."6a63f1b2-c070-4d70-99dd-d9199fb15962__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."6a63f1b2-c070-4d70-99dd-d9199fb15962__id_seq" OWNER TO ckan_default;

--
-- Name: 6a63f1b2-c070-4d70-99dd-d9199fb15962__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."6a63f1b2-c070-4d70-99dd-d9199fb15962__id_seq" OWNED BY public."6a63f1b2-c070-4d70-99dd-d9199fb15962"._id;


--
-- Name: 749530b7-97b3-47e5-a215-03b2ad0848bc; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."749530b7-97b3-47e5-a215-03b2ad0848bc" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Judul" text,
    "OPD" text,
    "Jumlah" numeric
);


ALTER TABLE public."749530b7-97b3-47e5-a215-03b2ad0848bc" OWNER TO ckan_default;

--
-- Name: 749530b7-97b3-47e5-a215-03b2ad0848bc__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."749530b7-97b3-47e5-a215-03b2ad0848bc__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."749530b7-97b3-47e5-a215-03b2ad0848bc__id_seq" OWNER TO ckan_default;

--
-- Name: 749530b7-97b3-47e5-a215-03b2ad0848bc__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."749530b7-97b3-47e5-a215-03b2ad0848bc__id_seq" OWNED BY public."749530b7-97b3-47e5-a215-03b2ad0848bc"._id;


--
-- Name: 83bb245e-17ff-4072-9520-c266964f34af; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."83bb245e-17ff-4072-9520-c266964f34af" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Tahun" numeric,
    "Laju Pertumbuhan Penduduk (%)" numeric
);


ALTER TABLE public."83bb245e-17ff-4072-9520-c266964f34af" OWNER TO ckan_default;

--
-- Name: 83bb245e-17ff-4072-9520-c266964f34af__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."83bb245e-17ff-4072-9520-c266964f34af__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."83bb245e-17ff-4072-9520-c266964f34af__id_seq" OWNER TO ckan_default;

--
-- Name: 83bb245e-17ff-4072-9520-c266964f34af__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."83bb245e-17ff-4072-9520-c266964f34af__id_seq" OWNED BY public."83bb245e-17ff-4072-9520-c266964f34af"._id;


--
-- Name: 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" (
    _id integer NOT NULL,
    _full_text tsvector,
    "No" text,
    "Instansi" text,
    "Islam" numeric,
    "Protestan" numeric,
    "Katolik" numeric,
    "Hindu" text,
    "Budha" numeric,
    "Konghucu" text,
    "Jumlah Total" numeric
);


ALTER TABLE public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" OWNER TO ckan_default;

--
-- Name: 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf__id_seq" OWNER TO ckan_default;

--
-- Name: 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf__id_seq" OWNED BY public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf"._id;


--
-- Name: 969f5bf6-82ea-48af-8972-7de5f7aab7ca; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Judul" text,
    "OPD" text,
    "Jumlah" numeric
);


ALTER TABLE public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" OWNER TO ckan_default;

--
-- Name: 969f5bf6-82ea-48af-8972-7de5f7aab7ca__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."969f5bf6-82ea-48af-8972-7de5f7aab7ca__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."969f5bf6-82ea-48af-8972-7de5f7aab7ca__id_seq" OWNER TO ckan_default;

--
-- Name: 969f5bf6-82ea-48af-8972-7de5f7aab7ca__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."969f5bf6-82ea-48af-8972-7de5f7aab7ca__id_seq" OWNED BY public."969f5bf6-82ea-48af-8972-7de5f7aab7ca"._id;


--
-- Name: _table_metadata; Type: VIEW; Schema: public; Owner: ckan_default
--

CREATE VIEW public._table_metadata AS
 SELECT DISTINCT substr(md5(((dependee.relname)::text || (COALESCE(dependent.relname, ''::name))::text)), 0, 17) AS _id,
    dependee.relname AS name,
    dependee.oid,
    dependent.relname AS alias_of
   FROM (((pg_class dependee
     LEFT JOIN pg_rewrite r ON ((r.ev_class = dependee.oid)))
     LEFT JOIN pg_depend d ON ((d.objid = r.oid)))
     LEFT JOIN pg_class dependent ON ((d.refobjid = dependent.oid)))
  WHERE (((dependee.oid <> dependent.oid) OR (dependent.oid IS NULL)) AND ((dependee.relkind = 'r'::"char") OR (dependee.relkind = 'v'::"char")) AND (dependee.relnamespace = ( SELECT pg_namespace.oid
           FROM pg_namespace
          WHERE (pg_namespace.nspname = 'public'::name))))
  ORDER BY dependee.oid DESC;


ALTER TABLE public._table_metadata OWNER TO ckan_default;

--
-- Name: a231f002-e113-45af-9a6a-6f7d6d2b09c7; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."a231f002-e113-45af-9a6a-6f7d6d2b09c7" (
    _id integer NOT NULL,
    _full_text tsvector,
    kelurahan_desa text,
    latitude numeric,
    longitude numeric,
    luas_panen numeric,
    produksi numeric,
    hasil numeric
);


ALTER TABLE public."a231f002-e113-45af-9a6a-6f7d6d2b09c7" OWNER TO ckan_default;

--
-- Name: COLUMN "a231f002-e113-45af-9a6a-6f7d6d2b09c7".kelurahan_desa; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."a231f002-e113-45af-9a6a-6f7d6d2b09c7".kelurahan_desa IS '{"notes": "Nama kelurahan atau desa.", "type_override": "text", "label": "Kelurahan/Desa"}';


--
-- Name: COLUMN "a231f002-e113-45af-9a6a-6f7d6d2b09c7".latitude; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."a231f002-e113-45af-9a6a-6f7d6d2b09c7".latitude IS '{"notes": "Titik latitude", "type_override": "numeric", "label": "Latitude"}';


--
-- Name: COLUMN "a231f002-e113-45af-9a6a-6f7d6d2b09c7".longitude; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."a231f002-e113-45af-9a6a-6f7d6d2b09c7".longitude IS '{"notes": "Titik longitude", "type_override": "numeric", "label": "Longitude"}';


--
-- Name: COLUMN "a231f002-e113-45af-9a6a-6f7d6d2b09c7".luas_panen; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."a231f002-e113-45af-9a6a-6f7d6d2b09c7".luas_panen IS '{"notes": "Luas panen dalam satuan hektar.", "type_override": "numeric", "label": "Luas Panen (Ha)"}';


--
-- Name: COLUMN "a231f002-e113-45af-9a6a-6f7d6d2b09c7".produksi; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."a231f002-e113-45af-9a6a-6f7d6d2b09c7".produksi IS '{"notes": "Total produksi dalam satuan kwintal.", "type_override": "numeric", "label": "Produksi (Kw)"}';


--
-- Name: COLUMN "a231f002-e113-45af-9a6a-6f7d6d2b09c7".hasil; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."a231f002-e113-45af-9a6a-6f7d6d2b09c7".hasil IS '{"notes": "Hasil panen dalam satuan kwintal per hektar", "type_override": "numeric", "label": "Hasil (Kw/Ha)"}';


--
-- Name: a231f002-e113-45af-9a6a-6f7d6d2b09c7__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."a231f002-e113-45af-9a6a-6f7d6d2b09c7__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."a231f002-e113-45af-9a6a-6f7d6d2b09c7__id_seq" OWNER TO ckan_default;

--
-- Name: a231f002-e113-45af-9a6a-6f7d6d2b09c7__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."a231f002-e113-45af-9a6a-6f7d6d2b09c7__id_seq" OWNED BY public."a231f002-e113-45af-9a6a-6f7d6d2b09c7"._id;


--
-- Name: b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Kelurahan/ Desa" text,
    "Latitude" numeric,
    "Longitude" numeric,
    "Luas Panen (Ha)" text,
    "Hasil (Kw /Ha)" text,
    "Produksi (Kw)" text
);


ALTER TABLE public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" OWNER TO ckan_default;

--
-- Name: b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851__id_seq" OWNER TO ckan_default;

--
-- Name: b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851__id_seq" OWNED BY public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851"._id;


--
-- Name: baa9b849-070f-42b4-ab61-cdde26a0ab68; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."baa9b849-070f-42b4-ab61-cdde26a0ab68" (
    _id integer NOT NULL,
    _full_text tsvector,
    "No" text,
    "Instansi" text,
    "Islam" numeric,
    "Protestan" numeric,
    "Katolik" numeric,
    "Hindu" text,
    "Budha" numeric,
    "Konghucu" text,
    "Jumlah Total" numeric
);


ALTER TABLE public."baa9b849-070f-42b4-ab61-cdde26a0ab68" OWNER TO ckan_default;

--
-- Name: baa9b849-070f-42b4-ab61-cdde26a0ab68__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."baa9b849-070f-42b4-ab61-cdde26a0ab68__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."baa9b849-070f-42b4-ab61-cdde26a0ab68__id_seq" OWNER TO ckan_default;

--
-- Name: baa9b849-070f-42b4-ab61-cdde26a0ab68__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."baa9b849-070f-42b4-ab61-cdde26a0ab68__id_seq" OWNED BY public."baa9b849-070f-42b4-ab61-cdde26a0ab68"._id;


--
-- Name: bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Judul" text,
    "OPD" text,
    "Jumlah" numeric
);


ALTER TABLE public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" OWNER TO ckan_default;

--
-- Name: bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171__id_seq" OWNER TO ckan_default;

--
-- Name: bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171__id_seq" OWNED BY public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171"._id;


--
-- Name: c9d86f58-fb73-4982-8268-c76c2c967853; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."c9d86f58-fb73-4982-8268-c76c2c967853" (
    _id integer NOT NULL,
    _full_text tsvector,
    "Kelurahan/Desa" text,
    "Latitude" text,
    "Longitude" text,
    "Luas (km2)" text,
    "Persentase terhadap Luas" text
);


ALTER TABLE public."c9d86f58-fb73-4982-8268-c76c2c967853" OWNER TO ckan_default;

--
-- Name: COLUMN "c9d86f58-fb73-4982-8268-c76c2c967853"."Kelurahan/Desa"; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."c9d86f58-fb73-4982-8268-c76c2c967853"."Kelurahan/Desa" IS '{"notes": "", "type_override": "text", "label": "Kelurahan/Desa"}';


--
-- Name: COLUMN "c9d86f58-fb73-4982-8268-c76c2c967853"."Latitude"; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."c9d86f58-fb73-4982-8268-c76c2c967853"."Latitude" IS '{"notes": "", "type_override": "numeric", "label": "Latitude"}';


--
-- Name: COLUMN "c9d86f58-fb73-4982-8268-c76c2c967853"."Longitude"; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."c9d86f58-fb73-4982-8268-c76c2c967853"."Longitude" IS '{"notes": "", "type_override": "numeric", "label": "Longitude"}';


--
-- Name: COLUMN "c9d86f58-fb73-4982-8268-c76c2c967853"."Luas (km2)"; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."c9d86f58-fb73-4982-8268-c76c2c967853"."Luas (km2)" IS '{"notes": "", "type_override": "numeric", "label": "Luas (km2)"}';


--
-- Name: COLUMN "c9d86f58-fb73-4982-8268-c76c2c967853"."Persentase terhadap Luas"; Type: COMMENT; Schema: public; Owner: ckan_default
--

COMMENT ON COLUMN public."c9d86f58-fb73-4982-8268-c76c2c967853"."Persentase terhadap Luas" IS '{"notes": "", "type_override": "numeric", "label": "Persentase terhadap Luas"}';


--
-- Name: c9d86f58-fb73-4982-8268-c76c2c967853__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."c9d86f58-fb73-4982-8268-c76c2c967853__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."c9d86f58-fb73-4982-8268-c76c2c967853__id_seq" OWNER TO ckan_default;

--
-- Name: c9d86f58-fb73-4982-8268-c76c2c967853__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."c9d86f58-fb73-4982-8268-c76c2c967853__id_seq" OWNED BY public."c9d86f58-fb73-4982-8268-c76c2c967853"._id;


--
-- Name: cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" (
    _id integer NOT NULL,
    _full_text tsvector,
    "NO." numeric,
    "RETRIBUSI" text,
    "2016" text,
    "2017" text
);


ALTER TABLE public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" OWNER TO ckan_default;

--
-- Name: cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb__id_seq" OWNER TO ckan_default;

--
-- Name: cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb__id_seq" OWNED BY public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb"._id;


--
-- Name: ec1f9576-dc94-4f88-bdc0-b79639b508b9; Type: TABLE; Schema: public; Owner: ckan_default
--

CREATE TABLE public."ec1f9576-dc94-4f88-bdc0-b79639b508b9" (
    _id integer NOT NULL,
    _full_text tsvector,
    "KECAMATAN" text,
    "DEGUNG" numeric,
    "JAIPONGAN" numeric,
    "GENJRING QOSIDAH" numeric,
    "CALUNG" numeric,
    "REOG" numeric
);


ALTER TABLE public."ec1f9576-dc94-4f88-bdc0-b79639b508b9" OWNER TO ckan_default;

--
-- Name: ec1f9576-dc94-4f88-bdc0-b79639b508b9__id_seq; Type: SEQUENCE; Schema: public; Owner: ckan_default
--

CREATE SEQUENCE public."ec1f9576-dc94-4f88-bdc0-b79639b508b9__id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ec1f9576-dc94-4f88-bdc0-b79639b508b9__id_seq" OWNER TO ckan_default;

--
-- Name: ec1f9576-dc94-4f88-bdc0-b79639b508b9__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ckan_default
--

ALTER SEQUENCE public."ec1f9576-dc94-4f88-bdc0-b79639b508b9__id_seq" OWNED BY public."ec1f9576-dc94-4f88-bdc0-b79639b508b9"._id;


--
-- Name: 012f05fe-7c10-4e53-85f7-5022636e571b _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."012f05fe-7c10-4e53-85f7-5022636e571b" ALTER COLUMN _id SET DEFAULT nextval('public."012f05fe-7c10-4e53-85f7-5022636e571b__id_seq"'::regclass);


--
-- Name: 0e2dd585-af1f-4098-8dbb-bc3916d3902c _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" ALTER COLUMN _id SET DEFAULT nextval('public."0e2dd585-af1f-4098-8dbb-bc3916d3902c__id_seq"'::regclass);


--
-- Name: 0f6b5cd2-14f9-424c-b1c2-b55e229789e9 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" ALTER COLUMN _id SET DEFAULT nextval('public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9__id_seq"'::regclass);


--
-- Name: 2592a627-a297-4997-89fb-203c76f83354 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."2592a627-a297-4997-89fb-203c76f83354" ALTER COLUMN _id SET DEFAULT nextval('public."2592a627-a297-4997-89fb-203c76f83354__id_seq"'::regclass);


--
-- Name: 284b5526-82fa-4213-bdbe-92e0d6909910 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."284b5526-82fa-4213-bdbe-92e0d6909910" ALTER COLUMN _id SET DEFAULT nextval('public."284b5526-82fa-4213-bdbe-92e0d6909910__id_seq"'::regclass);


--
-- Name: 2a59fc35-d9ce-4543-94b1-921e30af80e8 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."2a59fc35-d9ce-4543-94b1-921e30af80e8" ALTER COLUMN _id SET DEFAULT nextval('public."2a59fc35-d9ce-4543-94b1-921e30af80e8__id_seq"'::regclass);


--
-- Name: 34b7eed5-191f-4459-9276-e0479b71b056 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."34b7eed5-191f-4459-9276-e0479b71b056" ALTER COLUMN _id SET DEFAULT nextval('public."34b7eed5-191f-4459-9276-e0479b71b056__id_seq"'::regclass);


--
-- Name: 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59" ALTER COLUMN _id SET DEFAULT nextval('public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59__id_seq"'::regclass);


--
-- Name: 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea" ALTER COLUMN _id SET DEFAULT nextval('public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea__id_seq"'::regclass);


--
-- Name: 5238a3cd-a295-4ba1-8c93-66632195f349 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."5238a3cd-a295-4ba1-8c93-66632195f349" ALTER COLUMN _id SET DEFAULT nextval('public."5238a3cd-a295-4ba1-8c93-66632195f349__id_seq"'::regclass);


--
-- Name: 6098fa82-136c-4b35-aa94-829626f5f98e _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."6098fa82-136c-4b35-aa94-829626f5f98e" ALTER COLUMN _id SET DEFAULT nextval('public."6098fa82-136c-4b35-aa94-829626f5f98e__id_seq"'::regclass);


--
-- Name: 677400e2-f613-437d-9957-7dbd2ebb8668 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."677400e2-f613-437d-9957-7dbd2ebb8668" ALTER COLUMN _id SET DEFAULT nextval('public."677400e2-f613-437d-9957-7dbd2ebb8668__id_seq"'::regclass);


--
-- Name: 6a63f1b2-c070-4d70-99dd-d9199fb15962 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."6a63f1b2-c070-4d70-99dd-d9199fb15962" ALTER COLUMN _id SET DEFAULT nextval('public."6a63f1b2-c070-4d70-99dd-d9199fb15962__id_seq"'::regclass);


--
-- Name: 749530b7-97b3-47e5-a215-03b2ad0848bc _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."749530b7-97b3-47e5-a215-03b2ad0848bc" ALTER COLUMN _id SET DEFAULT nextval('public."749530b7-97b3-47e5-a215-03b2ad0848bc__id_seq"'::regclass);


--
-- Name: 83bb245e-17ff-4072-9520-c266964f34af _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."83bb245e-17ff-4072-9520-c266964f34af" ALTER COLUMN _id SET DEFAULT nextval('public."83bb245e-17ff-4072-9520-c266964f34af__id_seq"'::regclass);


--
-- Name: 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" ALTER COLUMN _id SET DEFAULT nextval('public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf__id_seq"'::regclass);


--
-- Name: 969f5bf6-82ea-48af-8972-7de5f7aab7ca _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" ALTER COLUMN _id SET DEFAULT nextval('public."969f5bf6-82ea-48af-8972-7de5f7aab7ca__id_seq"'::regclass);


--
-- Name: a231f002-e113-45af-9a6a-6f7d6d2b09c7 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."a231f002-e113-45af-9a6a-6f7d6d2b09c7" ALTER COLUMN _id SET DEFAULT nextval('public."a231f002-e113-45af-9a6a-6f7d6d2b09c7__id_seq"'::regclass);


--
-- Name: b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" ALTER COLUMN _id SET DEFAULT nextval('public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851__id_seq"'::regclass);


--
-- Name: baa9b849-070f-42b4-ab61-cdde26a0ab68 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."baa9b849-070f-42b4-ab61-cdde26a0ab68" ALTER COLUMN _id SET DEFAULT nextval('public."baa9b849-070f-42b4-ab61-cdde26a0ab68__id_seq"'::regclass);


--
-- Name: bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" ALTER COLUMN _id SET DEFAULT nextval('public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171__id_seq"'::regclass);


--
-- Name: c9d86f58-fb73-4982-8268-c76c2c967853 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."c9d86f58-fb73-4982-8268-c76c2c967853" ALTER COLUMN _id SET DEFAULT nextval('public."c9d86f58-fb73-4982-8268-c76c2c967853__id_seq"'::regclass);


--
-- Name: cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" ALTER COLUMN _id SET DEFAULT nextval('public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb__id_seq"'::regclass);


--
-- Name: ec1f9576-dc94-4f88-bdc0-b79639b508b9 _id; Type: DEFAULT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."ec1f9576-dc94-4f88-bdc0-b79639b508b9" ALTER COLUMN _id SET DEFAULT nextval('public."ec1f9576-dc94-4f88-bdc0-b79639b508b9__id_seq"'::regclass);


--
-- Data for Name: 012f05fe-7c10-4e53-85f7-5022636e571b; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."012f05fe-7c10-4e53-85f7-5022636e571b" (_id, _full_text, "Kelurahan/Desa", "Latitude", "Longitude", "Ketinggian") FROM stdin;
1	'108.238.367':3 '7.081.724':2 '850':4 'tenggerraharja':1	TENGGERRAHARJA	-7.081.724	108.238.367	850
2	'108.282.626':3 '7.087.020':2 '782':4 'sukamantri':1	SUKAMANTRI	-7.087.020	108.282.626	782
3	'108.295':3 '7.088.526':2 '827':4 'cibeureum':1	CIBEUREUM	-7.088.526	108.295	827
4	'108.320.898':3 '7.093.428':2 '720':4 'sindanglaya':1	SINDANGLAYA	-7.093.428	108.320.898	720
5	'108.343.328':3 '7.092.894':2 '700':4 'mekarwangi':1	MEKARWANGI	-7.092.894	108.343.328	700
6	'108.279.413':4 '7.089.797':3 '775':5 '8':6 'kec':1 'sukamantri':2	KEC. SUKAMANTRI	-7.089.797	108.279.413	775,8
\.


--
-- Data for Name: 0e2dd585-af1f-4098-8dbb-bc3916d3902c; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" (_id, _full_text, "APBD TAHUN", "APBD", "JUMLAH", "REALISASI") FROM stdin;
1	'2016':1 '4.841.385.024.00':7 '7.605.654.912.00':5 'daerah':3 'pendapatan':2 'rp':4,6	2016	Pendapatan Daerah	Rp. 7.605.654.912.00 	Rp. 4.841.385.024.00 
2	'2016':1 '70.309.924.244':7 '88.030.677.040.34':5 'belanja':2 'daerah':3 'rp':4,6	2016	Belanja Daerah	Rp. 88.030.677.040.34 	Rp. 70.309.924.244 
3	'2017':1 '8.197.910.853':7 '9.033.022.288':5 'daerah':3 'pendapatan':2 'rp':4,6	2017	Pendapatan Daerah	Rp. 9.033.022.288 	Rp. 8.197.910.853 
4	'2017':1 '52.051.259.581':7 '70.211.080.876':5 'belanja':2 'daerah':3 'rp':4,6	2017	Belanja Daerah	Rp. 70.211.080.876 	Rp. 52.051.259.581 
\.


--
-- Data for Name: 0f6b5cd2-14f9-424c-b1c2-b55e229789e9; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" (_id, _full_text, "No", "Instansi", "Islam", "Protestan", "Katolik", "Hindu", "Budha", "Konghucu", "Jumlah Total") FROM stdin;
1	'0':5,6,8 '0.0':7,9 '1.0':1 '179':4,10 'daerah':3 'sekretariat':2	1.0	Sekretariat Daerah	179	0	0	0.0	0	0.0	179
2	'0':5,6,8 '0.0':7,9 '2.0':1 '52':4,10 'dprd':3 'sekretariat':2	2.0	Sekretariat DPRD	52	0	0	0.0	0	0.0	52
3	'0':4,5,7 '0.0':6,8 '3.0':1 '49':3,9 'inspektorat':2	3.0	Inspektorat	49	0	0	0.0	0	0.0	49
4	'0':8,9,11 '0.0':10,12 '174':7,13 '4.0':1 'dan':4 'dina':2 'ketahanan':5 'pangan':6 'pertanian':3	4.0	Dinas Pertanian dan Ketahanan Pangan	174	0	0	0.0	0	0.0	174
5	'0':13,14,16 '0.0':15,17 '109':12,18 '5.0':1 'anak':11 'berencana':6 'dan':9 'dina':2 'keluarga':5 'pemberdayaan':7 'penduduk':4 'pengendalian':3 'perempuan':8 'perlindungan':10	5.0	Dinas Pengendalian Penduduk, Keluarga Berencana, Pemberdayaan Perempuan dan Perlindungan Anak	109	0	0	0.0	0	0.0	109
6	'0':8,9,11 '0.0':10,12 '34':7,13 '6.0':1 'dan':5 'dina':2 'kebudayaan':3 'kepemudaan':4 'olahraga':6	6.0	Dinas Kebudayaan, Kepemudaan dan Olahraga	34	0	0	0.0	0	0.0	34
7	'0':10,11,13 '0.0':12,14 '68':9,15 '7.0':1 'dan':7 'dina':2 'kecil':5 'koperasi':3 'menengah':6 'perdagangan':8 'usaha':4	7.0	Dinas Koperasi, Usaha Kecil, Menengah dan Perdagangan	68	0	0	0.0	0	0.0	68
8	'0':5,6,8 '0.0':7,9 '39':4,10 '8.0':1 'dina':2 'pariwisata':3	8.0	Dinas Pariwisata	39	0	0	0.0	0	0.0	39
9	'0':5,6,8 '0.0':7,9 '28':4,10 '9.0':1 'dina':2 'sosial':3	9.0	Dinas Sosial	28	0	0	0.0	0	0.0	28
10	'0':8 '0.0':7,9 '1':6 '10.0':1 '1110':4 '1117':10 '6':5 'dina':2 'kesehatan':3	10.0	Dinas Kesehatan	1110	6	1	0.0	0	0.0	1117
11	'0':6,8 '0.0':7,9 '11.0':1 '6905':4 '6912':10 '7':5 'dina':2 'pendidikan':3	11.0	Dinas Pendidikan	6905	7	0	0.0	0	0.0	6912
12	'0':10,11,13 '0.0':12,14 '12.0':1 '185':9,15 'dan':7 'dina':2 'pekerjaan':3 'penataan':5 'pertanahan':8 'ruang':6 'umum':4	12.0	Dinas Pekerjaan Umum, Penataan Ruang dan Pertanahan	185	0	0	0.0	0	0.0	185
13	'0':5,6,8 '0.0':7,9 '108':4,10 '13.0':1 'dina':2 'perhubungan':3	13.0	Dinas Perhubungan	108	0	0	0.0	0	0.0	108
14	'0':11,12,14 '0.0':13,15 '14.0':1 '33':10,16 'dan':5 'dina':2 'modal':4 'pelayanan':6 'penanaman':3 'pintu':9 'satu':8 'terpadu':7	14.0	Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu	33	0	0	0.0	0	0.0	33
15	'0':7,8,10 '0.0':9,11 '15.0':1 '68':6,12 'dan':4 'dina':2 'perikanan':5 'peternakan':3	15.0	Dinas Peternakan dan Perikanan	68	0	0	0.0	0	0.0	68
16	'0':11,12,14 '0.0':13,15 '16.0':1 '182':10,16 'dan':7 'dina':2 'hidup':9 'kawasan':5 'lingkungan':8 'permukiman':6 'perumahan':3 'rakyat':4	16.0	Dinas Perumahan Rakyat, Kawasan Permukiman dan Lingkungan Hidup	182	0	0	0.0	0	0.0	182
17	'0':8,9,11 '0.0':10,12 '17.0':1 '61':7,13 'dan':4 'dina':2 'kependudukan':3 'pencatatan':5 'sipil':6	17.0	Dinas Kependudukan dan Pencatatan Sipil	61	0	0	0.0	0	0.0	61
18	'0':7,8,10 '0.0':9,11 '18.0':1 '23':6,12 'dan':4 'dina':2 'informatika':5 'komunikasi':3	18.0	Dinas Komunikasi dan Informatika	23	0	0	0.0	0	0.0	23
19	'0':6,7,9 '0.0':8,10 '19.0':1 '29':5,11 'dina':2 'kerja':4 'tenaga':3	19.0	Dinas Tenaga Kerja	29	0	0	0.0	0	0.0	29
20	'0':7,8,10 '0.0':9,11 '20.0':1 '28':6,12 'dan':4 'dina':2 'kearsipan':5 'perpustakaan':3	20.0	Dinas Perpustakaan dan Kearsipan	28	0	0	0.0	0	0.0	28
21	'0':8,9,11 '0.0':10,12 '21.0':1 '29':7,13 'dan':5 'desa':6 'dina':2 'masyarakat':4 'pemberdayaan':3	21.0	Dinas Pemberdayaan Masyarakat dan Desa	29	0	0	0.0	0	0.0	29
22	'0':7,8,10 '0.0':9,11 '135':6,12 '22.0':1 'badan':2 'daerah':5 'keuangan':4 'pengelolaan':3	22.0	Badan Pengelolaan Keuangan Daerah	135	0	0	0.0	0	0.0	135
23	'0':7,8,10 '0.0':9,11 '23.0':1 '41':6,12 'badan':2 'daerah':5 'pembangunan':4 'perencanaan':3	23.0	Badan Perencanaan Pembangunan Daerah	41	0	0	0.0	0	0.0	41
24	'0':11,13 '0.0':12,14 '1':10 '24.0':1 '57':9 '58':15 'badan':2 'dan':4 'daya':7 'kepegawaian':3 'manusia':8 'pengembangan':5 'sumber':6	24.0	Badan Kepegawaian dan Pengembangan Sumber Daya Manusia	57	1	0	0.0	0	0.0	58
25	'0':7,8,10 '0.0':9,11 '22':6,12 '25.0':1 'badan':2 'bencana':4 'daerah':5 'penanggulangan':3	25.0	Badan Penanggulangan Bencana Daerah	22	0	0	0.0	0	0.0	22
26	'0':7,8 '0.0':9,11 '1':10 '26.0':1 '378':6 '379':12 'daerah':5 'rumah':2 'sakit':3 'umum':4	26.0	Rumah Sakit Umum Daerah	378	0	0	0.0	1	0.0	379
27	'0':8,9,11 '0.0':10,12 '15':7,13 '27.0':1 'bangsa':4 'dan':5 'kantor':2 'kesatuan':3 'politik':6	27.0	Kantor Kesatuan Bangsa dan Politik	15	0	0	0.0	0	0.0	15
28	'0':7,8,10 '0.0':9,11 '28.0':1 '58':6,12 'pamong':4 'polisi':3 'praja':5 'satuan':2	28.0	Satuan Polisi Pamong Praja	58	0	0	0.0	0	0.0	58
29	'0':7,8,10 '0.0':9,11 '29.0':1 '8':6,12 'badan':2 'dpk':5 'narkotika':3 'nasion':4	29.0	Badan Narkotika Nasional (DPK)	8	0	0	0.0	0	0.0	8
30	'0':5,7 '0.0':6,8 '1':4 '30.0':1 '445':3 '446':9 'kecamatan':2	30.0	Kecamatan	445	1	0	0.0	0	0.0	446
31	'0':7,8,10 '0.0':9,11 '31.0':1 '7':6,12 'komisi':3 'pemilihan':4 'sekretariat':2 'umum':5	31.0	Sekretariat Komisi Pemilihan Umum	7	0	0	0.0	0	0.0	7
32	'0.0':5,7 '1':4,6 '10659':2 '10676':8 '15':3 'jumlah':1		Jumlah	10659	15	1	0.0	1	0.0	10676
33				\N	\N	\N		\N		\N
34	'badan':2 'ciami':10 'dan':4 'daya':7 'kab':9 'kepegawaian':3 'manusia':8 'pengembangan':5 'sumber':1,6	Sumber: Badan Kepegawaian dan Pengembangan Sumber Daya Manusia Kab. Ciamis		\N	\N	\N		\N		\N
\.


--
-- Data for Name: 2592a627-a297-4997-89fb-203c76f83354; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."2592a627-a297-4997-89fb-203c76f83354" (_id, _full_text, tahun, akses_air_bersih, akses_jamban) FROM stdin;
1	'2017':1 '72.61':3 '82.20':2	2017	82.20	72.61
2	'2018':1 '73.06':3 '89.12':2	2018	89.12	73.06
3	'2019':1 '67.38':3 '89.02':2	2019	89.02	67.38
\.


--
-- Data for Name: 284b5526-82fa-4213-bdbe-92e0d6909910; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."284b5526-82fa-4213-bdbe-92e0d6909910" (_id, _full_text, "Judul", "OPD", "Jumlah") FROM stdin;
1	'1':3 'disdik':2	A	Disdik	1
2	'2':3 'b':1 'bkpsdm':2	B	BKPSDM	2
\.


--
-- Data for Name: 2a59fc35-d9ce-4543-94b1-921e30af80e8; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."2a59fc35-d9ce-4543-94b1-921e30af80e8" (_id, _full_text, "Tahun", "Harapan Hidup (%)") FROM stdin;
1	'2012':1 '48':3 '68':2	2012	68,48
2	'2013':1 '60':3 '68':2	2013	68,60
3	'2014':1 '66':3 '68':2	2014	68,66
4	'06':3 '2015':1 '69':2	2015	69,06
5	'2016':1 '22':3 '69':2	2016	69,22
\.


--
-- Data for Name: 34b7eed5-191f-4459-9276-e0479b71b056; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."34b7eed5-191f-4459-9276-e0479b71b056" (_id, _full_text, "Judul", "OPD", "Jumlah") FROM stdin;
1	'1':3 'disdik':2	A	Disdik	1
2	'2':3 'b':1 'bkpsdm':2	B	BKPSDM	2
\.


--
-- Data for Name: 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59" (_id, _full_text, "No", "Nilai Eksport Berdasarkan Negara Tujuan", "Jumlah (Rp)") FROM stdin;
1	'1':1 '9763136.02':4 'amerika':3 'negara':2	1	Negara Amerika	9763136.02
2	'2':1 '4420372.40':6 'eropa':5 'negara':3,4 'negara-negara':2	2	Negara-negara Eropa	4420372.40
3	'2438.40':7 '3':1 'asia':5 'barat':6 'negara':3,4 'negara-negara':2	3	Negara-negara Asia Barat	2438.40
4	'2658496.64':7 '4':1 'asia':5 'negara':3,4 'negara-negara':2 'timur':6	4	Negara-negara Asia Timur	2658496.64
5	'32307.69':7 '5':1 'asia':5 'negara':3,4 'negara-negara':2 'tenggara':6	5	Negara-negara Asia Tenggara	32307.69
6	'231582.15':7 '6':1 'australia':3 'dan':4 'negara':2 'new':5 'zealand':6	6	Negara Australia dan New Zealand	231582.15
\.


--
-- Data for Name: 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea" (_id, _full_text, "No", "Jenis Investasi", "Nilai") FROM stdin;
1	'0':7 '1':1 'crop':5 'pangan':3 'perkebunan/food':4 'plantat':6 'tanaman':2	1	Tanaman Pangan & Perkebunan/Food Crops & Plantation	0
2	'2':1 '328775000':3 'peternakan/livestock':2	2	Peternakan/Livestock	328775000
3	'0':3 '3':1 'kehutanan/forestry':2	3	Kehutanan/Forestry	0
4	'0':3 '4':1 'perikanan/fishery':2	4	Perikanan/Fishery	0
5	'0':3 '5':1 'pertambangan/mining':2	5	Pertambangan/Mining	0
\.


--
-- Data for Name: 5238a3cd-a295-4ba1-8c93-66632195f349; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."5238a3cd-a295-4ba1-8c93-66632195f349" (_id, _full_text, "No", "Indikator", "2017", "2018", "2019", "2020") FROM stdin;
1	'1':1 '340414':6 '341322':7 '344998':8 '347196':9 'huni':4 'layak':3 'rumah':2 'unit':5	1	Rumah Layak Huni (unit)	340414	341322	344998	347196
2	'2':1 '93.2':6 '93.5':7 '94.5':8 '94.9':9 'huni':5 'layak':4 'rasio':2 'rumah':3	2	Rasio Rumah Layak Huni (%)	93.2	93.5	94.5	94.9
\.


--
-- Data for Name: 6098fa82-136c-4b35-aa94-829626f5f98e; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."6098fa82-136c-4b35-aa94-829626f5f98e" (_id, _full_text, "Tahun", "Laju Pertumbuhan Penduduk (%)") FROM stdin;
1	'0':2 '2017':1 '561':3	2017	0,561
2	'0':2 '2018':1 '562':3	2018	0,562
3	'0':2 '2019':1 '551':3	2019	0,551
4		\N	
5		\N	
6		\N	
7		\N	
8		\N	
9		\N	
10		\N	
11		\N	
12		\N	
13		\N	
\.


--
-- Data for Name: 677400e2-f613-437d-9957-7dbd2ebb8668; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."677400e2-f613-437d-9957-7dbd2ebb8668" (_id, _full_text, "No.", "Capaian", "2017", "2018", "2019", "Satuan") FROM stdin;
1	'1':1 '32':9,10,11 'instansi':7 'jabatan':3 'jumlah':2 'orang':12 'pada':6 'pemerintah':8 'pimpinan':4 'tinggi':5	1	Jumlah Jabatan Pimpinan Tinggi pada instansi pemerintah	32	32	32	orang
2	'2':1 '24':10 '27':9 '32':8 'jabatan':3 'jumlah':2 'orang':11 'pimpinan':4 'terisi':7 'tinggi':5 'yang':6	2	Jumlah Jabatan Pimpinan Tinggi yang terisi	32	27	24	orang
3	'1018':8,9,10 '3':1 'administrasi':4 'instansi':6 'jabatan':3 'jumlah':2 'orang':11 'pada':5 'pemerintah':7	3	Jumlah jabatan Administrasi pada instansi pemerintah	1018	1018	1018	orang
4	'4':1 '888':11 '895':12 '991':10 'administrasi':4 'instansi':6 'jabatan':3 'jumlah':2 'orang':13 'pada':5 'pemerintah':7 'terisi':9 'yang':8	4	Jumlah jabatan Administrasi pada instansi pemerintah yang terisi	991	888	895	orang
5	'5':1 '6768':9 '7024':8 '7622':7 'fungsion':5 'jabatan':4 'jumlah':2 'orang':10 'pemangku':3 'tertentu':6	5	Jumlah pemangku jabatan fungsional tertentu	7622	7024	6768	orang
6	'58.65':7 '6':1 '60.02':8 '60.71':9 'berijazah':3 'persen':10 'pns':2 's1':4 's2':5 's3':6	6	PNS berijazah S1, S2, S3	58.65	60.02	60.71	persen
7	'11':10,11,12 '7':1 'dan':8 'hari':13 'lama':4 'mendapatkan':6 'pegawai':5 'pelatihan':9 'pendidikan':7 'rata':2,3	7	Rata - rata lama pegawai mendapatkan pendidikan dan pelatihan	11	11	11	hari
8	'1.19':10 '2.19':11 '27.04':12 '8':1 'asn':3 'dan':7 'formal':9 'mengikuti':5 'pelatihan':8 'pendidikan':6 'persen':13 'persentas':2 'yang':4	8	Persentase ASN yang mengikuti pendidikan dan pelatihan formal	1.19	2.19	27.04	persen
9	'34.47':12 '40.00':11 '66.6':10 '9':1 'asn':3 'dan':7 'mengikuti':5 'pelatihan':8 'pendidikan':6 'persen':13 'persentas':2 'struktur':9 'yang':4	9	Persentase ASN yang mengikuti pendidikan dan pelatihan struktural	66.6	40.00	34.47	persen
10	'10':1 '1240':6 '2584':7 '282':5 'dalam':3 'diklat':4 'keiukutsertaan':2 'orang':8	10	Keiukutsertaan dalam diklat	282	1240	2584	orang
11	'11':1 '165':17 '237':18 '529':19 'bagi':12 'dan':10,14 'diklat':6 'fungsion':11 'jumlah':2 'mengikuti':5 'non':15 'orang':20 'pegawai':3 'perjenjangan':7 'pns':13,16 'struktur':8 'tekni':9 'yang':4	11	Jumlah Pegawai yang mengikuti diklat perjenjangan struktural, teknis dan fungsional bagi PNS dan Non PNS	165	237	529	orang
12	'0':9 '117':8 '12':1 '194':10 'diklat':6 'jumlah':2 'mengikuti':5 'orang':11 'pns':3 'prajabatan':7 'yang':4	12	Jumlah PNS yang mengikuti Diklat Prajabatan	117	0	194	orang
13	'13':1 '679':9 '706':10 '709':11 'dan':7 'jumlah':2 'orang':12 'pensiun':8 'pindah':5 'pns':3 'tuga':6 'yang':4	13	Jumlah PNS yang pindah tugas dan pensiun	679	706	709	orang
14	'14':1 '2':11,12,13 'anggaran':7 'aparatur':10 'dan':6 'data':4 'jumlah':2 'karier':9 'kegiatan':14 'kepegawaian':5 'pengelolaan':3 'pengembangan':8	14	Jumlah pengelolaan data kepegawaian dan anggaran pengembangan karier aparatur	2	2	2	kegiatan
15	'118':9 '13':7 '15':1 '400':8 'jumlah':2 'mengikuti':5 'orang':10 'pegawai':3 'pembinaan':6 'yang':4	15	Jumlah pegawai yang mengikuti pembinaan	13	400	118	orang
16				\N	\N	\N	
17	'bkpsdm':2 'ciami':4 'kabupaten':3 'sumber':1	Sumber : BKPSDM Kabupaten Ciamis		\N	\N	\N	
\.


--
-- Data for Name: 6a63f1b2-c070-4d70-99dd-d9199fb15962; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."6a63f1b2-c070-4d70-99dd-d9199fb15962" (_id, _full_text, "Judul", "OPD", "Jumlah") FROM stdin;
1	'1':3 'disdik':2	A	Disdik	1
2	'2':3 'b':1 'bkpsdm':2	B	BKPSDM	2
\.


--
-- Data for Name: 749530b7-97b3-47e5-a215-03b2ad0848bc; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."749530b7-97b3-47e5-a215-03b2ad0848bc" (_id, _full_text, "Judul", "OPD", "Jumlah") FROM stdin;
1	'1':3 'disdik':2	A	Disdik	1
2	'2':3 'b':1 'bkpsdm':2	B	BKPSDM	2
\.


--
-- Data for Name: 83bb245e-17ff-4072-9520-c266964f34af; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."83bb245e-17ff-4072-9520-c266964f34af" (_id, _full_text, "Tahun", "Laju Pertumbuhan Penduduk (%)") FROM stdin;
1	'0.561':2 '2017':1	2017	0.561
2	'0.562':2 '2018':1	2018	0.562
3	'0.551':2 '2019':1	2019	0.551
4	\N	\N	\N
5	\N	\N	\N
6	\N	\N	\N
7	\N	\N	\N
8	\N	\N	\N
9	\N	\N	\N
10	\N	\N	\N
11	\N	\N	\N
12	\N	\N	\N
13	\N	\N	\N
\.


--
-- Data for Name: 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" (_id, _full_text, "No", "Instansi", "Islam", "Protestan", "Katolik", "Hindu", "Budha", "Konghucu", "Jumlah Total") FROM stdin;
1	'0':5,6,8 '0.0':7,9 '1.0':1 '179':4,10 'daerah':3 'sekretariat':2	1.0	Sekretariat Daerah	179	0	0	0.0	0	0.0	179
2	'0':5,6,8 '0.0':7,9 '2.0':1 '52':4,10 'dprd':3 'sekretariat':2	2.0	Sekretariat DPRD	52	0	0	0.0	0	0.0	52
3	'0':4,5,7 '0.0':6,8 '3.0':1 '49':3,9 'inspektorat':2	3.0	Inspektorat	49	0	0	0.0	0	0.0	49
4	'0':8,9,11 '0.0':10,12 '174':7,13 '4.0':1 'dan':4 'dina':2 'ketahanan':5 'pangan':6 'pertanian':3	4.0	Dinas Pertanian dan Ketahanan Pangan	174	0	0	0.0	0	0.0	174
5	'0':13,14,16 '0.0':15,17 '109':12,18 '5.0':1 'anak':11 'berencana':6 'dan':9 'dina':2 'keluarga':5 'pemberdayaan':7 'penduduk':4 'pengendalian':3 'perempuan':8 'perlindungan':10	5.0	Dinas Pengendalian Penduduk, Keluarga Berencana, Pemberdayaan Perempuan dan Perlindungan Anak	109	0	0	0.0	0	0.0	109
6	'0':8,9,11 '0.0':10,12 '34':7,13 '6.0':1 'dan':5 'dina':2 'kebudayaan':3 'kepemudaan':4 'olahraga':6	6.0	Dinas Kebudayaan, Kepemudaan dan Olahraga	34	0	0	0.0	0	0.0	34
7	'0':10,11,13 '0.0':12,14 '68':9,15 '7.0':1 'dan':7 'dina':2 'kecil':5 'koperasi':3 'menengah':6 'perdagangan':8 'usaha':4	7.0	Dinas Koperasi, Usaha Kecil, Menengah dan Perdagangan	68	0	0	0.0	0	0.0	68
8	'0':5,6,8 '0.0':7,9 '39':4,10 '8.0':1 'dina':2 'pariwisata':3	8.0	Dinas Pariwisata	39	0	0	0.0	0	0.0	39
9	'0':5,6,8 '0.0':7,9 '28':4,10 '9.0':1 'dina':2 'sosial':3	9.0	Dinas Sosial	28	0	0	0.0	0	0.0	28
10	'0':8 '0.0':7,9 '1':6 '10.0':1 '1110':4 '1117':10 '6':5 'dina':2 'kesehatan':3	10.0	Dinas Kesehatan	1110	6	1	0.0	0	0.0	1117
11	'0':6,8 '0.0':7,9 '11.0':1 '6905':4 '6912':10 '7':5 'dina':2 'pendidikan':3	11.0	Dinas Pendidikan	6905	7	0	0.0	0	0.0	6912
12	'0':10,11,13 '0.0':12,14 '12.0':1 '185':9,15 'dan':7 'dina':2 'pekerjaan':3 'penataan':5 'pertanahan':8 'ruang':6 'umum':4	12.0	Dinas Pekerjaan Umum, Penataan Ruang dan Pertanahan	185	0	0	0.0	0	0.0	185
13	'0':5,6,8 '0.0':7,9 '108':4,10 '13.0':1 'dina':2 'perhubungan':3	13.0	Dinas Perhubungan	108	0	0	0.0	0	0.0	108
14	'0':11,12,14 '0.0':13,15 '14.0':1 '33':10,16 'dan':5 'dina':2 'modal':4 'pelayanan':6 'penanaman':3 'pintu':9 'satu':8 'terpadu':7	14.0	Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu	33	0	0	0.0	0	0.0	33
15	'0':7,8,10 '0.0':9,11 '15.0':1 '68':6,12 'dan':4 'dina':2 'perikanan':5 'peternakan':3	15.0	Dinas Peternakan dan Perikanan	68	0	0	0.0	0	0.0	68
16	'0':11,12,14 '0.0':13,15 '16.0':1 '182':10,16 'dan':7 'dina':2 'hidup':9 'kawasan':5 'lingkungan':8 'permukiman':6 'perumahan':3 'rakyat':4	16.0	Dinas Perumahan Rakyat, Kawasan Permukiman dan Lingkungan Hidup	182	0	0	0.0	0	0.0	182
17	'0':8,9,11 '0.0':10,12 '17.0':1 '61':7,13 'dan':4 'dina':2 'kependudukan':3 'pencatatan':5 'sipil':6	17.0	Dinas Kependudukan dan Pencatatan Sipil	61	0	0	0.0	0	0.0	61
18	'0':7,8,10 '0.0':9,11 '18.0':1 '23':6,12 'dan':4 'dina':2 'informatika':5 'komunikasi':3	18.0	Dinas Komunikasi dan Informatika	23	0	0	0.0	0	0.0	23
19	'0':6,7,9 '0.0':8,10 '19.0':1 '29':5,11 'dina':2 'kerja':4 'tenaga':3	19.0	Dinas Tenaga Kerja	29	0	0	0.0	0	0.0	29
20	'0':7,8,10 '0.0':9,11 '20.0':1 '28':6,12 'dan':4 'dina':2 'kearsipan':5 'perpustakaan':3	20.0	Dinas Perpustakaan dan Kearsipan	28	0	0	0.0	0	0.0	28
21	'0':8,9,11 '0.0':10,12 '21.0':1 '29':7,13 'dan':5 'desa':6 'dina':2 'masyarakat':4 'pemberdayaan':3	21.0	Dinas Pemberdayaan Masyarakat dan Desa	29	0	0	0.0	0	0.0	29
22	'0':7,8,10 '0.0':9,11 '135':6,12 '22.0':1 'badan':2 'daerah':5 'keuangan':4 'pengelolaan':3	22.0	Badan Pengelolaan Keuangan Daerah	135	0	0	0.0	0	0.0	135
23	'0':7,8,10 '0.0':9,11 '23.0':1 '41':6,12 'badan':2 'daerah':5 'pembangunan':4 'perencanaan':3	23.0	Badan Perencanaan Pembangunan Daerah	41	0	0	0.0	0	0.0	41
24	'0':11,13 '0.0':12,14 '1':10 '24.0':1 '57':9 '58':15 'badan':2 'dan':4 'daya':7 'kepegawaian':3 'manusia':8 'pengembangan':5 'sumber':6	24.0	Badan Kepegawaian dan Pengembangan Sumber Daya Manusia	57	1	0	0.0	0	0.0	58
25	'0':7,8,10 '0.0':9,11 '22':6,12 '25.0':1 'badan':2 'bencana':4 'daerah':5 'penanggulangan':3	25.0	Badan Penanggulangan Bencana Daerah	22	0	0	0.0	0	0.0	22
26	'0':7,8 '0.0':9,11 '1':10 '26.0':1 '378':6 '379':12 'daerah':5 'rumah':2 'sakit':3 'umum':4	26.0	Rumah Sakit Umum Daerah	378	0	0	0.0	1	0.0	379
27	'0':8,9,11 '0.0':10,12 '15':7,13 '27.0':1 'bangsa':4 'dan':5 'kantor':2 'kesatuan':3 'politik':6	27.0	Kantor Kesatuan Bangsa dan Politik	15	0	0	0.0	0	0.0	15
28	'0':7,8,10 '0.0':9,11 '28.0':1 '58':6,12 'pamong':4 'polisi':3 'praja':5 'satuan':2	28.0	Satuan Polisi Pamong Praja	58	0	0	0.0	0	0.0	58
29	'0':7,8,10 '0.0':9,11 '29.0':1 '8':6,12 'badan':2 'dpk':5 'narkotika':3 'nasion':4	29.0	Badan Narkotika Nasional (DPK)	8	0	0	0.0	0	0.0	8
30	'0':5,7 '0.0':6,8 '1':4 '30.0':1 '445':3 '446':9 'kecamatan':2	30.0	Kecamatan	445	1	0	0.0	0	0.0	446
31	'0':7,8,10 '0.0':9,11 '31.0':1 '7':6,12 'komisi':3 'pemilihan':4 'sekretariat':2 'umum':5	31.0	Sekretariat Komisi Pemilihan Umum	7	0	0	0.0	0	0.0	7
32	'0.0':5,7 '1':4,6 '10659':2 '10676':8 '15':3 'jumlah':1		Jumlah	10659	15	1	0.0	1	0.0	10676
33				\N	\N	\N		\N		\N
34	'badan':2 'ciami':10 'dan':4 'daya':7 'kab':9 'kepegawaian':3 'manusia':8 'pengembangan':5 'sumber':1,6	Sumber: Badan Kepegawaian dan Pengembangan Sumber Daya Manusia Kab. Ciamis		\N	\N	\N		\N		\N
\.


--
-- Data for Name: 969f5bf6-82ea-48af-8972-7de5f7aab7ca; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" (_id, _full_text, "Judul", "OPD", "Jumlah") FROM stdin;
1	'1':3 'disdik':2	A	Disdik	1
2	'2':3 'b':1 'bkpsdm':2	B	BKPSDM	2
\.


--
-- Data for Name: a231f002-e113-45af-9a6a-6f7d6d2b09c7; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."a231f002-e113-45af-9a6a-6f7d6d2b09c7" (_id, _full_text, kelurahan_desa, latitude, longitude, luas_panen, produksi, hasil) FROM stdin;
1	'-7.316003317696471':3 '1.69':5 '10.97':6 '108.29885715019753':4 '64.94':7 'imbanagara':1 'raya':2	Imbanagara Raya	-7.316003317696471	108.29885715019753	1.69	10.97	64.94
2	'-7.309958740594564':2 '108.30482237903016':3 '13.67':5 '2.11':4 '64.80':6 'cisadap':1	Cisadap	-7.309958740594564	108.30482237903016	2.11	13.67	64.80
3	'-7.317183669110256':2 '108.30660396924293':3 '14.75':5 '2.27':4 '64.96':6 'imbanagara':1	Imbanagara	-7.317183669110256	108.30660396924293	2.27	14.75	64.96
4	'-7.3208718406918845':2 '1.90':4 '108.33264734852737':3 '12.33':5 '64.90':6 'sindangrasa':1	Sindangrasa	-7.3208718406918845	108.33264734852737	1.90	12.33	64.90
5	'-7.32662328234459':2 '0.43':4 '108.31578079586033':3 '2.80':5 '65.08':6 'panyingkiran':1	Panyingkiran	-7.32662328234459	108.31578079586033	0.43	2.80	65.08
6	'-7.333559126912277':2 '1.06':4 '108.32862522654413':3 '6.87':5 '64.82':6 'pawindan':1	Pawindan	-7.333559126912277	108.32862522654413	1.06	6.87	64.82
7	'-7.336949538877002':2 '0.80':4 '108.3417036344926':3 '5.20':5 '65.00':6 'linggasari':1	Linggasari	-7.336949538877002	108.3417036344926	0.80	5.20	65.00
8	'-7.328246376750493':2 '0.42':4 '108.35516998606363':3 '2.73':5 '64.90':6 'ciami':1	Ciamis	-7.328246376750493	108.35516998606363	0.42	2.73	64.90
9	'-7.339131846794594':2 '0.91':4 '108.3539144925526':3 '5.90':5 '64.86':6 'benteng':1	Benteng	-7.339131846794594	108.3539144925526	0.91	5.90	64.86
10	'-7.338932389418084':2 '0.69':4 '108.37574450781202':3 '4.49':5 '65.05':6 'cigembor':1	Cigembor	-7.338932389418084	108.37574450781202	0.69	4.49	65.05
11	'-7.3295651009512':2 '0.68':4 '108.36749896249627':3 '4.45':5 '65.02':6 'kertasari':1	Kertasari	-7.3295651009512	108.36749896249627	0.68	4.45	65.02
12	'-7.321981012984842':2 '0.44':4 '108.34871553152847':3 '2.85':5 '64.85':6 'maleb':1	Maleber	-7.321981012984842	108.34871553152847	0.44	2.85	64.85
\.


--
-- Data for Name: b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" (_id, _full_text, "Kelurahan/ Desa", "Latitude", "Longitude", "Luas Panen (Ha)", "Hasil (Kw /Ha)", "Produksi (Kw)") FROM stdin;
1	'-7.316003317696471':3 '0':5 '108.29885715019753':4 '3':9 '40':7 '54':8 '97':10 '98':6 'imbanagara':1 'raya':2	Imbanagara Raya	-7.316003317696471	108.29885715019753	0,98	40,54	3,97
2	'-7.309958740594564':2 '0':4 '108.30482237903016':3 '3':8 '40':6 '52':7 '69':9 '91':5 'cisadap':1	Cisadap	-7.309958740594564	108.30482237903016	0,91	40,52	3,69
3	'-7.317183669110256':2 '0':4 '1':8 '108.30660396924293':3 '40':6 '45':7 '48':5 '94':9 'imbanagara':1	Imbanagara	-7.317183669110256	108.30660396924293	0,48	40,45	1,94
4	'-7.3208718406918845':2 '1':4 '108.33264734852737':3 '12':5 '4':8 '40':6 '5':7 '54':9 'sindangrasa':1	Sindangrasa	-7.3208718406918845	108.33264734852737	1,12	40,5	4,54
5	'-7.32662328234459':2 '1':4 '108.31578079586033':3 '11':9 '26':5 '40':6 '5':8 '52':7 'panyingkiran':1	Panyingkiran	-7.32662328234459	108.31578079586033	1,26	40,52	5,11
6	'-7.333559126912277':2 '0':4 '1':8 '108.32862522654413':3 '40':6 '42':5 '55':7 '7':9 'pawindan':1	Pawindan	-7.333559126912277	108.32862522654413	0,42	40,55	1,7
7	'-7.336949538877002':2 '0':4 '108.3417036344926':3 '2':8 '40':6 '44':7 '55':9 '63':5 'linggasari':1	Linggasari	-7.336949538877002	108.3417036344926	0,63	40,44	2,55
8	'-7.328246376750493':2 '108.35516998606363':3 '37':5 '40':4 'ciami':1	Ciamis	-7.328246376750493	108.35516998606363		40,37	
9	'-7.339131846794594':2 '108.3539144925526':3 '40':4 '56':5 'benteng':1	Benteng	-7.339131846794594	108.3539144925526		40,56	
10	'-7.338932389418084':2 '108.37574450781202':3 '48':4 '5':5 'cigembor':1	Cigembor	-7.338932389418084	108.37574450781202		48,5	
11	'-7.3295651009512':2 '0':4 '03':9 '108.36749896249627':3 '2':8 '40':6 '5':5 '52':7 'kertasari':1	Kertasari	-7.3295651009512	108.36749896249627	0,5	40,52	2,03
12	'-7.321981012984842':2 '0':4 '1':8 '108.34871553152847':3 '14':9 '28':5 '40':6 '6':7 'maleb':1	Maleber	-7.321981012984842	108.34871553152847	0,28	40,6	1,14
13			\N	\N			
14			\N	\N			
\.


--
-- Data for Name: baa9b849-070f-42b4-ab61-cdde26a0ab68; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."baa9b849-070f-42b4-ab61-cdde26a0ab68" (_id, _full_text, "No", "Instansi", "Islam", "Protestan", "Katolik", "Hindu", "Budha", "Konghucu", "Jumlah Total") FROM stdin;
1	'0':5,6,8 '0.0':7,9 '1.0':1 '179':4,10 'daerah':3 'sekretariat':2	1.0	Sekretariat Daerah	179	0	0	0.0	0	0.0	179
2	'0':5,6,8 '0.0':7,9 '2.0':1 '52':4,10 'dprd':3 'sekretariat':2	2.0	Sekretariat DPRD	52	0	0	0.0	0	0.0	52
3	'0':4,5,7 '0.0':6,8 '3.0':1 '49':3,9 'inspektorat':2	3.0	Inspektorat	49	0	0	0.0	0	0.0	49
4	'0':8,9,11 '0.0':10,12 '174':7,13 '4.0':1 'dan':4 'dina':2 'ketahanan':5 'pangan':6 'pertanian':3	4.0	Dinas Pertanian dan Ketahanan Pangan	174	0	0	0.0	0	0.0	174
5	'0':13,14,16 '0.0':15,17 '109':12,18 '5.0':1 'anak':11 'berencana':6 'dan':9 'dina':2 'keluarga':5 'pemberdayaan':7 'penduduk':4 'pengendalian':3 'perempuan':8 'perlindungan':10	5.0	Dinas Pengendalian Penduduk, Keluarga Berencana, Pemberdayaan Perempuan dan Perlindungan Anak	109	0	0	0.0	0	0.0	109
6	'0':8,9,11 '0.0':10,12 '34':7,13 '6.0':1 'dan':5 'dina':2 'kebudayaan':3 'kepemudaan':4 'olahraga':6	6.0	Dinas Kebudayaan, Kepemudaan dan Olahraga	34	0	0	0.0	0	0.0	34
7	'0':10,11,13 '0.0':12,14 '68':9,15 '7.0':1 'dan':7 'dina':2 'kecil':5 'koperasi':3 'menengah':6 'perdagangan':8 'usaha':4	7.0	Dinas Koperasi, Usaha Kecil, Menengah dan Perdagangan	68	0	0	0.0	0	0.0	68
8	'0':5,6,8 '0.0':7,9 '39':4,10 '8.0':1 'dina':2 'pariwisata':3	8.0	Dinas Pariwisata	39	0	0	0.0	0	0.0	39
9	'0':5,6,8 '0.0':7,9 '28':4,10 '9.0':1 'dina':2 'sosial':3	9.0	Dinas Sosial	28	0	0	0.0	0	0.0	28
10	'0':8 '0.0':7,9 '1':6 '10.0':1 '1110':4 '1117':10 '6':5 'dina':2 'kesehatan':3	10.0	Dinas Kesehatan	1110	6	1	0.0	0	0.0	1117
11	'0':6,8 '0.0':7,9 '11.0':1 '6905':4 '6912':10 '7':5 'dina':2 'pendidikan':3	11.0	Dinas Pendidikan	6905	7	0	0.0	0	0.0	6912
12	'0':10,11,13 '0.0':12,14 '12.0':1 '185':9,15 'dan':7 'dina':2 'pekerjaan':3 'penataan':5 'pertanahan':8 'ruang':6 'umum':4	12.0	Dinas Pekerjaan Umum, Penataan Ruang dan Pertanahan	185	0	0	0.0	0	0.0	185
13	'0':5,6,8 '0.0':7,9 '108':4,10 '13.0':1 'dina':2 'perhubungan':3	13.0	Dinas Perhubungan	108	0	0	0.0	0	0.0	108
14	'0':11,12,14 '0.0':13,15 '14.0':1 '33':10,16 'dan':5 'dina':2 'modal':4 'pelayanan':6 'penanaman':3 'pintu':9 'satu':8 'terpadu':7	14.0	Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu	33	0	0	0.0	0	0.0	33
15	'0':7,8,10 '0.0':9,11 '15.0':1 '68':6,12 'dan':4 'dina':2 'perikanan':5 'peternakan':3	15.0	Dinas Peternakan dan Perikanan	68	0	0	0.0	0	0.0	68
16	'0':11,12,14 '0.0':13,15 '16.0':1 '182':10,16 'dan':7 'dina':2 'hidup':9 'kawasan':5 'lingkungan':8 'permukiman':6 'perumahan':3 'rakyat':4	16.0	Dinas Perumahan Rakyat, Kawasan Permukiman dan Lingkungan Hidup	182	0	0	0.0	0	0.0	182
17	'0':8,9,11 '0.0':10,12 '17.0':1 '61':7,13 'dan':4 'dina':2 'kependudukan':3 'pencatatan':5 'sipil':6	17.0	Dinas Kependudukan dan Pencatatan Sipil	61	0	0	0.0	0	0.0	61
18	'0':7,8,10 '0.0':9,11 '18.0':1 '23':6,12 'dan':4 'dina':2 'informatika':5 'komunikasi':3	18.0	Dinas Komunikasi dan Informatika	23	0	0	0.0	0	0.0	23
19	'0':6,7,9 '0.0':8,10 '19.0':1 '29':5,11 'dina':2 'kerja':4 'tenaga':3	19.0	Dinas Tenaga Kerja	29	0	0	0.0	0	0.0	29
20	'0':7,8,10 '0.0':9,11 '20.0':1 '28':6,12 'dan':4 'dina':2 'kearsipan':5 'perpustakaan':3	20.0	Dinas Perpustakaan dan Kearsipan	28	0	0	0.0	0	0.0	28
21	'0':8,9,11 '0.0':10,12 '21.0':1 '29':7,13 'dan':5 'desa':6 'dina':2 'masyarakat':4 'pemberdayaan':3	21.0	Dinas Pemberdayaan Masyarakat dan Desa	29	0	0	0.0	0	0.0	29
22	'0':7,8,10 '0.0':9,11 '135':6,12 '22.0':1 'badan':2 'daerah':5 'keuangan':4 'pengelolaan':3	22.0	Badan Pengelolaan Keuangan Daerah	135	0	0	0.0	0	0.0	135
23	'0':7,8,10 '0.0':9,11 '23.0':1 '41':6,12 'badan':2 'daerah':5 'pembangunan':4 'perencanaan':3	23.0	Badan Perencanaan Pembangunan Daerah	41	0	0	0.0	0	0.0	41
24	'0':11,13 '0.0':12,14 '1':10 '24.0':1 '57':9 '58':15 'badan':2 'dan':4 'daya':7 'kepegawaian':3 'manusia':8 'pengembangan':5 'sumber':6	24.0	Badan Kepegawaian dan Pengembangan Sumber Daya Manusia	57	1	0	0.0	0	0.0	58
25	'0':7,8,10 '0.0':9,11 '22':6,12 '25.0':1 'badan':2 'bencana':4 'daerah':5 'penanggulangan':3	25.0	Badan Penanggulangan Bencana Daerah	22	0	0	0.0	0	0.0	22
26	'0':7,8 '0.0':9,11 '1':10 '26.0':1 '378':6 '379':12 'daerah':5 'rumah':2 'sakit':3 'umum':4	26.0	Rumah Sakit Umum Daerah	378	0	0	0.0	1	0.0	379
27	'0':8,9,11 '0.0':10,12 '15':7,13 '27.0':1 'bangsa':4 'dan':5 'kantor':2 'kesatuan':3 'politik':6	27.0	Kantor Kesatuan Bangsa dan Politik	15	0	0	0.0	0	0.0	15
28	'0':7,8,10 '0.0':9,11 '28.0':1 '58':6,12 'pamong':4 'polisi':3 'praja':5 'satuan':2	28.0	Satuan Polisi Pamong Praja	58	0	0	0.0	0	0.0	58
29	'0':7,8,10 '0.0':9,11 '29.0':1 '8':6,12 'badan':2 'dpk':5 'narkotika':3 'nasion':4	29.0	Badan Narkotika Nasional (DPK)	8	0	0	0.0	0	0.0	8
30	'0':5,7 '0.0':6,8 '1':4 '30.0':1 '445':3 '446':9 'kecamatan':2	30.0	Kecamatan	445	1	0	0.0	0	0.0	446
31	'0':7,8,10 '0.0':9,11 '31.0':1 '7':6,12 'komisi':3 'pemilihan':4 'sekretariat':2 'umum':5	31.0	Sekretariat Komisi Pemilihan Umum	7	0	0	0.0	0	0.0	7
32	'0.0':5,7 '1':4,6 '10659':2 '10676':8 '15':3 'jumlah':1		Jumlah	10659	15	1	0.0	1	0.0	10676
33				\N	\N	\N		\N		\N
34	'badan':2 'ciami':10 'dan':4 'daya':7 'kab':9 'kepegawaian':3 'manusia':8 'pengembangan':5 'sumber':1,6	Sumber: Badan Kepegawaian dan Pengembangan Sumber Daya Manusia Kab. Ciamis		\N	\N	\N		\N		\N
\.


--
-- Data for Name: bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" (_id, _full_text, "Judul", "OPD", "Jumlah") FROM stdin;
1	'1':3 'disdik':2	A	Disdik	1
2	'2':3 'b':1 'bkpsdm':2	B	BKPSDM	2
\.


--
-- Data for Name: c9d86f58-fb73-4982-8268-c76c2c967853; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."c9d86f58-fb73-4982-8268-c76c2c967853" (_id, _full_text, "Kelurahan/Desa", "Latitude", "Longitude", "Luas (km2)", "Persentase terhadap Luas") FROM stdin;
1	'108.238.367':3 '17':6 '52':5 '7.081.724':2 '79':7 '8':4 'tenggerraharja':1	TENGGERRAHARJA	-7.081.724	108.238.367	8,52	17,79
2	'108.282.626':3 '12':7 '17':6 '2':5 '7.087.020':2 '8':4 'sukamantri':1	SUKAMANTRI	-7.087.020	108.282.626	8,2	17,12
3	'108.295.106':3 '14':4 '24':7 '30':6 '48':5 '7.088.526':2 'cibeureum':1	CIBEUREUM	-7.088.526	108.295.106	14,48	30,24
4	'108.320.898':3 '15':6 '5':5 '66':7 '7':4 '7.093.428':2 'sindanglaya':1	SINDANGLAYA	-7.093.428	108.320.898	7,5	15,66
5	'108.343.328':3 '17':7 '18':5 '19':6 '7.092.894':2 '9':4 'mekarwangi':1	MEKARWANGI	-7.092.894	108.343.328	9,18	19,17
6	'100':7 '108.279.413':4 '47':5 '7.089.797':3 '88':6 'kec':1 'sukamantri':2	KEC. SUKAMANTRI	-7.089.797	108.279.413	47,88	100
\.


--
-- Data for Name: cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" (_id, _full_text, "NO.", "RETRIBUSI", "2016", "2017") FROM stdin;
1	'1':1 '170.430.000':4 '49.860.000':5 'izin':2 'trayek':3	1	IZIN TRAYEK	170.430.000 	49.860.000
2	'1.287.468.700':5 '1.651.880.400':6 '2':1 'bangunan':4 'izin':2 'mendiirkan':3	2	IZIN MENDIIRKAN BANGUNAN	1.287.468.700 	1.651.880.400
3	'3':1 '320.000.000':7 '93.750.000':6 'bangunan':4 'izin':2 'mendiirkan':3 'tower':5	3	IZIN MENDIIRKAN BANGUNAN TOWER	93.750.000 	320.000.000
4	'1.089.715.800':5 '1.919.723.500':4 '4':1 'gangguan':3 'izin':2	4	IZIN GANGGUAN	1.919.723.500 	1.089.715.800
5	'0':7 '5':1 '977.065.800':8 'ase':6 'izin':2 'kerja':5 'mempekerjakan':3 'tenaga':4	5	IZIN MEMPEKERJAKAN TENAGA KERJA ASING	0	977.065.800
6	'3.471.372.200':3 '4.088.522.000':4 '6':1 'jumlah':2	6	JUMLAH	3.471.372.200	4.088.522.000
\.


--
-- Data for Name: ec1f9576-dc94-4f88-bdc0-b79639b508b9; Type: TABLE DATA; Schema: public; Owner: ckan_default
--

COPY public."ec1f9576-dc94-4f88-bdc0-b79639b508b9" (_id, _full_text, "KECAMATAN", "DEGUNG", "JAIPONGAN", "GENJRING QOSIDAH", "CALUNG", "REOG") FROM stdin;
1	'0':2,5,6 '2':3,4 'lemahsugih':1	Lemahsugih	0	2	2	0	0
2	'0':5,6 '1':2,4 '2':3 'bantarujeg':1	Bantarujeg	1	2	1	0	0
3	'0':2,3,4,5,6 'malausma':1	Malausma	0	0	0	0	0
4	'0':2,4,5,6 '1':3 'cikij':1	Cikijing	0	1	0	0	0
5	'0':2,3,4,5,6 'cingambul':1	Cingambul	0	0	0	0	0
6	'0':2,4,5,6 '1':3 'talaga':1	Talaga	0	1	0	0	0
7	'0':2,3,4,5,6 'banjaran':1	Banjaran	0	0	0	0	0
8	'0':2,5,6 '2':4 '4':3 'argapura':1	Argapura	0	4	2	0	0
9	'0':2,5,6 '1':4 '2':3 'maja':1	Maja	0	2	1	0	0
10	'0':6 '1':5 '2':2 '4':4 '8':3 'majalengka':1	Majalengka	2	8	4	1	0
11	'0':4,6 '1':5 '2':2 '4':3 'cigasong':1	Cigasong	2	4	0	1	0
12	'0':2,5,6 '2':3,4 'sukahaji':1	Sukahaji	0	2	2	0	0
13	'0':2,4,5,6 '1':3 'sindang':1	Sindang	0	1	0	0	0
14	'0':4,5,6 '1':2 '4':3 'rajagaluh':1	Rajagaluh	1	4	0	0	0
15	'0':2,4,5,6 '2':3 'sindangwangi':1	Sindangwangi	0	2	0	0	0
16	'0':2,4,5,6 '2':3 'leuwimund':1	Leuwimunding	0	2	0	0	0
17	'0':2,4,5,6 '8':3 'palasah':1	Palasah	0	8	0	0	0
18	'0':4,5,6 '2':2 '5':3 'jatiwangi':1	Jatiwangi	2	5	0	0	0
19	'0':2,5 '1':6 '2':4 '7':3 'dawuan':1	Dawuan	0	7	2	0	1
20	'0':2,5,6 '2':4 '4':3 'kasokandel':1	Kasokandel	0	4	2	0	0
21	'0':2,4,5,6 '2':3 'panyingkiran':1	Panyingkiran	0	2	0	0	0
22	'0':2,4,5,6 '4':3 'kadipaten':1	Kadipaten	0	4	0	0	0
23	'0':2,3,4,5,6 'kertajati':1	Kertajati	0	0	0	0	0
24	'0':2,3,4,5,6 'jatitujuh':1	Jatitujuh	0	0	0	0	0
25	'0':4,5,6 '1':2 '3':3 'ligung':1	Ligung	1	3	0	0	0
26	'0':2,5,6 '1':3,4 'sumberjaya':1	Sumberjaya	0	1	1	0	0
27	'1':6 '17':4 '2':5 '69':3 '9':2 'jumlah':1	Jumlah	9	69	17	2	1
\.


--
-- Name: 012f05fe-7c10-4e53-85f7-5022636e571b__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."012f05fe-7c10-4e53-85f7-5022636e571b__id_seq"', 6, true);


--
-- Name: 0e2dd585-af1f-4098-8dbb-bc3916d3902c__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."0e2dd585-af1f-4098-8dbb-bc3916d3902c__id_seq"', 4, true);


--
-- Name: 0f6b5cd2-14f9-424c-b1c2-b55e229789e9__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9__id_seq"', 34, true);


--
-- Name: 2592a627-a297-4997-89fb-203c76f83354__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."2592a627-a297-4997-89fb-203c76f83354__id_seq"', 3, true);


--
-- Name: 284b5526-82fa-4213-bdbe-92e0d6909910__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."284b5526-82fa-4213-bdbe-92e0d6909910__id_seq"', 2, true);


--
-- Name: 2a59fc35-d9ce-4543-94b1-921e30af80e8__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."2a59fc35-d9ce-4543-94b1-921e30af80e8__id_seq"', 5, true);


--
-- Name: 34b7eed5-191f-4459-9276-e0479b71b056__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."34b7eed5-191f-4459-9276-e0479b71b056__id_seq"', 2, true);


--
-- Name: 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59__id_seq"', 6, true);


--
-- Name: 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea__id_seq"', 5, true);


--
-- Name: 5238a3cd-a295-4ba1-8c93-66632195f349__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."5238a3cd-a295-4ba1-8c93-66632195f349__id_seq"', 2, true);


--
-- Name: 6098fa82-136c-4b35-aa94-829626f5f98e__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."6098fa82-136c-4b35-aa94-829626f5f98e__id_seq"', 13, true);


--
-- Name: 677400e2-f613-437d-9957-7dbd2ebb8668__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."677400e2-f613-437d-9957-7dbd2ebb8668__id_seq"', 17, true);


--
-- Name: 6a63f1b2-c070-4d70-99dd-d9199fb15962__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."6a63f1b2-c070-4d70-99dd-d9199fb15962__id_seq"', 2, true);


--
-- Name: 749530b7-97b3-47e5-a215-03b2ad0848bc__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."749530b7-97b3-47e5-a215-03b2ad0848bc__id_seq"', 2, true);


--
-- Name: 83bb245e-17ff-4072-9520-c266964f34af__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."83bb245e-17ff-4072-9520-c266964f34af__id_seq"', 13, true);


--
-- Name: 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf__id_seq"', 34, true);


--
-- Name: 969f5bf6-82ea-48af-8972-7de5f7aab7ca__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."969f5bf6-82ea-48af-8972-7de5f7aab7ca__id_seq"', 2, true);


--
-- Name: a231f002-e113-45af-9a6a-6f7d6d2b09c7__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."a231f002-e113-45af-9a6a-6f7d6d2b09c7__id_seq"', 12, true);


--
-- Name: b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851__id_seq"', 14, true);


--
-- Name: baa9b849-070f-42b4-ab61-cdde26a0ab68__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."baa9b849-070f-42b4-ab61-cdde26a0ab68__id_seq"', 34, true);


--
-- Name: bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171__id_seq"', 2, true);


--
-- Name: c9d86f58-fb73-4982-8268-c76c2c967853__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."c9d86f58-fb73-4982-8268-c76c2c967853__id_seq"', 6, true);


--
-- Name: cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb__id_seq"', 6, true);


--
-- Name: ec1f9576-dc94-4f88-bdc0-b79639b508b9__id_seq; Type: SEQUENCE SET; Schema: public; Owner: ckan_default
--

SELECT pg_catalog.setval('public."ec1f9576-dc94-4f88-bdc0-b79639b508b9__id_seq"', 27, true);


--
-- Name: 012f05fe-7c10-4e53-85f7-5022636e571b 012f05fe-7c10-4e53-85f7-5022636e571b_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."012f05fe-7c10-4e53-85f7-5022636e571b"
    ADD CONSTRAINT "012f05fe-7c10-4e53-85f7-5022636e571b_pkey" PRIMARY KEY (_id);


--
-- Name: 0e2dd585-af1f-4098-8dbb-bc3916d3902c 0e2dd585-af1f-4098-8dbb-bc3916d3902c_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."0e2dd585-af1f-4098-8dbb-bc3916d3902c"
    ADD CONSTRAINT "0e2dd585-af1f-4098-8dbb-bc3916d3902c_pkey" PRIMARY KEY (_id);


--
-- Name: 0f6b5cd2-14f9-424c-b1c2-b55e229789e9 0f6b5cd2-14f9-424c-b1c2-b55e229789e9_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9"
    ADD CONSTRAINT "0f6b5cd2-14f9-424c-b1c2-b55e229789e9_pkey" PRIMARY KEY (_id);


--
-- Name: 2592a627-a297-4997-89fb-203c76f83354 2592a627-a297-4997-89fb-203c76f83354_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."2592a627-a297-4997-89fb-203c76f83354"
    ADD CONSTRAINT "2592a627-a297-4997-89fb-203c76f83354_pkey" PRIMARY KEY (_id);


--
-- Name: 284b5526-82fa-4213-bdbe-92e0d6909910 284b5526-82fa-4213-bdbe-92e0d6909910_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."284b5526-82fa-4213-bdbe-92e0d6909910"
    ADD CONSTRAINT "284b5526-82fa-4213-bdbe-92e0d6909910_pkey" PRIMARY KEY (_id);


--
-- Name: 2a59fc35-d9ce-4543-94b1-921e30af80e8 2a59fc35-d9ce-4543-94b1-921e30af80e8_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."2a59fc35-d9ce-4543-94b1-921e30af80e8"
    ADD CONSTRAINT "2a59fc35-d9ce-4543-94b1-921e30af80e8_pkey" PRIMARY KEY (_id);


--
-- Name: 34b7eed5-191f-4459-9276-e0479b71b056 34b7eed5-191f-4459-9276-e0479b71b056_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."34b7eed5-191f-4459-9276-e0479b71b056"
    ADD CONSTRAINT "34b7eed5-191f-4459-9276-e0479b71b056_pkey" PRIMARY KEY (_id);


--
-- Name: 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59"
    ADD CONSTRAINT "44e7c9ac-76e7-48d2-ae92-766bd4cbfd59_pkey" PRIMARY KEY (_id);


--
-- Name: 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea"
    ADD CONSTRAINT "4a71e1cf-05bd-4ddc-9f58-defe9cb984ea_pkey" PRIMARY KEY (_id);


--
-- Name: 5238a3cd-a295-4ba1-8c93-66632195f349 5238a3cd-a295-4ba1-8c93-66632195f349_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."5238a3cd-a295-4ba1-8c93-66632195f349"
    ADD CONSTRAINT "5238a3cd-a295-4ba1-8c93-66632195f349_pkey" PRIMARY KEY (_id);


--
-- Name: 6098fa82-136c-4b35-aa94-829626f5f98e 6098fa82-136c-4b35-aa94-829626f5f98e_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."6098fa82-136c-4b35-aa94-829626f5f98e"
    ADD CONSTRAINT "6098fa82-136c-4b35-aa94-829626f5f98e_pkey" PRIMARY KEY (_id);


--
-- Name: 677400e2-f613-437d-9957-7dbd2ebb8668 677400e2-f613-437d-9957-7dbd2ebb8668_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."677400e2-f613-437d-9957-7dbd2ebb8668"
    ADD CONSTRAINT "677400e2-f613-437d-9957-7dbd2ebb8668_pkey" PRIMARY KEY (_id);


--
-- Name: 6a63f1b2-c070-4d70-99dd-d9199fb15962 6a63f1b2-c070-4d70-99dd-d9199fb15962_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."6a63f1b2-c070-4d70-99dd-d9199fb15962"
    ADD CONSTRAINT "6a63f1b2-c070-4d70-99dd-d9199fb15962_pkey" PRIMARY KEY (_id);


--
-- Name: 749530b7-97b3-47e5-a215-03b2ad0848bc 749530b7-97b3-47e5-a215-03b2ad0848bc_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."749530b7-97b3-47e5-a215-03b2ad0848bc"
    ADD CONSTRAINT "749530b7-97b3-47e5-a215-03b2ad0848bc_pkey" PRIMARY KEY (_id);


--
-- Name: 83bb245e-17ff-4072-9520-c266964f34af 83bb245e-17ff-4072-9520-c266964f34af_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."83bb245e-17ff-4072-9520-c266964f34af"
    ADD CONSTRAINT "83bb245e-17ff-4072-9520-c266964f34af_pkey" PRIMARY KEY (_id);


--
-- Name: 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf"
    ADD CONSTRAINT "8c35e110-c18d-4ed0-adcd-5e5ee1e7debf_pkey" PRIMARY KEY (_id);


--
-- Name: 969f5bf6-82ea-48af-8972-7de5f7aab7ca 969f5bf6-82ea-48af-8972-7de5f7aab7ca_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."969f5bf6-82ea-48af-8972-7de5f7aab7ca"
    ADD CONSTRAINT "969f5bf6-82ea-48af-8972-7de5f7aab7ca_pkey" PRIMARY KEY (_id);


--
-- Name: a231f002-e113-45af-9a6a-6f7d6d2b09c7 a231f002-e113-45af-9a6a-6f7d6d2b09c7_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."a231f002-e113-45af-9a6a-6f7d6d2b09c7"
    ADD CONSTRAINT "a231f002-e113-45af-9a6a-6f7d6d2b09c7_pkey" PRIMARY KEY (_id);


--
-- Name: b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851 b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851"
    ADD CONSTRAINT "b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851_pkey" PRIMARY KEY (_id);


--
-- Name: baa9b849-070f-42b4-ab61-cdde26a0ab68 baa9b849-070f-42b4-ab61-cdde26a0ab68_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."baa9b849-070f-42b4-ab61-cdde26a0ab68"
    ADD CONSTRAINT "baa9b849-070f-42b4-ab61-cdde26a0ab68_pkey" PRIMARY KEY (_id);


--
-- Name: bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171 bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171"
    ADD CONSTRAINT "bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171_pkey" PRIMARY KEY (_id);


--
-- Name: c9d86f58-fb73-4982-8268-c76c2c967853 c9d86f58-fb73-4982-8268-c76c2c967853_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."c9d86f58-fb73-4982-8268-c76c2c967853"
    ADD CONSTRAINT "c9d86f58-fb73-4982-8268-c76c2c967853_pkey" PRIMARY KEY (_id);


--
-- Name: cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb"
    ADD CONSTRAINT "cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb_pkey" PRIMARY KEY (_id);


--
-- Name: ec1f9576-dc94-4f88-bdc0-b79639b508b9 ec1f9576-dc94-4f88-bdc0-b79639b508b9_pkey; Type: CONSTRAINT; Schema: public; Owner: ckan_default
--

ALTER TABLE ONLY public."ec1f9576-dc94-4f88-bdc0-b79639b508b9"
    ADD CONSTRAINT "ec1f9576-dc94-4f88-bdc0-b79639b508b9_pkey" PRIMARY KEY (_id);


--
-- Name: 0099a1885c11f700ff11460fee2061d5332190d0; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "0099a1885c11f700ff11460fee2061d5332190d0" ON public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" USING gist (to_tsvector('english'::regconfig, "Konghucu"));


--
-- Name: 02c5f41a226b3e8b9903c5ef37d19c7fe8de3de8; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "02c5f41a226b3e8b9903c5ef37d19c7fe8de3de8" ON public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" USING gist (to_tsvector('english'::regconfig, "Kelurahan/ Desa"));


--
-- Name: 08756ea2e37be37ce7935234d90f6c83284948ce; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "08756ea2e37be37ce7935234d90f6c83284948ce" ON public."34b7eed5-191f-4459-9276-e0479b71b056" USING gist (_full_text);


--
-- Name: 121d7d48269503c061f976117c1e0cc2dd02570e; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "121d7d48269503c061f976117c1e0cc2dd02570e" ON public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" USING gist (_full_text);


--
-- Name: 1417f41f354f8acb0cf6fa1530368987b420a5f3; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "1417f41f354f8acb0cf6fa1530368987b420a5f3" ON public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" USING gist (_full_text);


--
-- Name: 17fcb38244bf68e0d29743cc358c70dba212c0c1; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "17fcb38244bf68e0d29743cc358c70dba212c0c1" ON public."284b5526-82fa-4213-bdbe-92e0d6909910" USING gist (to_tsvector('english'::regconfig, "Judul"));


--
-- Name: 1b3004a3c74470c791b0d10ced6f54e206ae007a; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "1b3004a3c74470c791b0d10ced6f54e206ae007a" ON public."2592a627-a297-4997-89fb-203c76f83354" USING gist (_full_text);


--
-- Name: 1b5f53f5e946e591856633905db3af161935d84c; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "1b5f53f5e946e591856633905db3af161935d84c" ON public."a231f002-e113-45af-9a6a-6f7d6d2b09c7" USING gist (to_tsvector('english'::regconfig, kelurahan_desa));


--
-- Name: 1e845622833ed9f81e63dbef39888656cd98074c; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "1e845622833ed9f81e63dbef39888656cd98074c" ON public."012f05fe-7c10-4e53-85f7-5022636e571b" USING gist (_full_text);


--
-- Name: 291ee69d4eed470cbdd3c145b7c8fa0e4a7abc9a; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "291ee69d4eed470cbdd3c145b7c8fa0e4a7abc9a" ON public."c9d86f58-fb73-4982-8268-c76c2c967853" USING gist (to_tsvector('english'::regconfig, "Longitude"));


--
-- Name: 2acb48262c3a6a4c4b71836e8f36ceedcac2d31e; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "2acb48262c3a6a4c4b71836e8f36ceedcac2d31e" ON public."677400e2-f613-437d-9957-7dbd2ebb8668" USING gist (to_tsvector('english'::regconfig, "Capaian"));


--
-- Name: 2aff2d3f5dde905e6a55e4cc073225139f881b26; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "2aff2d3f5dde905e6a55e4cc073225139f881b26" ON public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea" USING gist (_full_text);


--
-- Name: 3ad7b8c5849093676404ca479dfe54d29d245c43; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "3ad7b8c5849093676404ca479dfe54d29d245c43" ON public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" USING gist (to_tsvector('english'::regconfig, "Judul"));


--
-- Name: 3cd463611662c29f155c9c29c92ad1375dc94c32; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "3cd463611662c29f155c9c29c92ad1375dc94c32" ON public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea" USING gist (to_tsvector('english'::regconfig, "Jenis Investasi"));


--
-- Name: 40ee6d8fec39376950be3afddbc0747384209ea8; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "40ee6d8fec39376950be3afddbc0747384209ea8" ON public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" USING gist (to_tsvector('english'::regconfig, "OPD"));


--
-- Name: 4100e8594028fb8f84ffaf4ae0fba2340594821e; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "4100e8594028fb8f84ffaf4ae0fba2340594821e" ON public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" USING gist (to_tsvector('english'::regconfig, "REALISASI"));


--
-- Name: 44828161428ac2c952bc5bd802c684ee79428f92; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "44828161428ac2c952bc5bd802c684ee79428f92" ON public."c9d86f58-fb73-4982-8268-c76c2c967853" USING gist (to_tsvector('english'::regconfig, "Latitude"));


--
-- Name: 463043c93eda5c3b593e3b1cef269afc9b3e35d3; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "463043c93eda5c3b593e3b1cef269afc9b3e35d3" ON public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" USING gist (to_tsvector('english'::regconfig, "Produksi (Kw)"));


--
-- Name: 466c2f658d0edc111a3b9f6bb14c87afe20e142e; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "466c2f658d0edc111a3b9f6bb14c87afe20e142e" ON public."baa9b849-070f-42b4-ab61-cdde26a0ab68" USING gist (to_tsvector('english'::regconfig, "No"));


--
-- Name: 487d8cbf107917ff62a886b8073e61ef81fc99a2; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "487d8cbf107917ff62a886b8073e61ef81fc99a2" ON public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59" USING gist (_full_text);


--
-- Name: 49beb10523c6dd91dedca22c1f0c6977231df67b; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "49beb10523c6dd91dedca22c1f0c6977231df67b" ON public."baa9b849-070f-42b4-ab61-cdde26a0ab68" USING gist (to_tsvector('english'::regconfig, "Hindu"));


--
-- Name: 49e8b2d1a04f3b443c85c6901f61ec504e93b0a8; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "49e8b2d1a04f3b443c85c6901f61ec504e93b0a8" ON public."5238a3cd-a295-4ba1-8c93-66632195f349" USING gist (to_tsvector('english'::regconfig, "Indikator"));


--
-- Name: 4bf192092d1397e537c989f914b97c8f479dc36a; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "4bf192092d1397e537c989f914b97c8f479dc36a" ON public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" USING gist (to_tsvector('english'::regconfig, "Konghucu"));


--
-- Name: 4cf0e1bb4fae61d77cab8f228fb4f4797db28e28; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "4cf0e1bb4fae61d77cab8f228fb4f4797db28e28" ON public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" USING gist (to_tsvector('english'::regconfig, "APBD"));


--
-- Name: 4df742b146bb528927de9c20eda503868df3a5a8; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "4df742b146bb528927de9c20eda503868df3a5a8" ON public."6a63f1b2-c070-4d70-99dd-d9199fb15962" USING gist (_full_text);


--
-- Name: 4dfd611d8215fcd667e4ae600f00a35d2bbcb481; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "4dfd611d8215fcd667e4ae600f00a35d2bbcb481" ON public."34b7eed5-191f-4459-9276-e0479b71b056" USING gist (to_tsvector('english'::regconfig, "Judul"));


--
-- Name: 504374ce8e0b7e1c9d0f499b7f6410a1a66202cb; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "504374ce8e0b7e1c9d0f499b7f6410a1a66202cb" ON public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" USING gist (to_tsvector('english'::regconfig, "Hindu"));


--
-- Name: 519ebe5feeabd4df3d0a3de5147a7497ee17e525; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "519ebe5feeabd4df3d0a3de5147a7497ee17e525" ON public."749530b7-97b3-47e5-a215-03b2ad0848bc" USING gist (_full_text);


--
-- Name: 52c01d2865477e2fbe43eb55a3e5e74cdb612fe4; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "52c01d2865477e2fbe43eb55a3e5e74cdb612fe4" ON public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" USING gist (to_tsvector('english'::regconfig, "Instansi"));


--
-- Name: 56ad32f42500a93fe182e636b22e1112a53ff6d8; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "56ad32f42500a93fe182e636b22e1112a53ff6d8" ON public."284b5526-82fa-4213-bdbe-92e0d6909910" USING gist (_full_text);


--
-- Name: 57c21cce3d70e54a399a0038f73648d377e12c32; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "57c21cce3d70e54a399a0038f73648d377e12c32" ON public."6098fa82-136c-4b35-aa94-829626f5f98e" USING gist (to_tsvector('english'::regconfig, "Laju Pertumbuhan Penduduk (%)"));


--
-- Name: 5bb4a508184b7f60399d258219ec1fe6547f29db; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "5bb4a508184b7f60399d258219ec1fe6547f29db" ON public."6098fa82-136c-4b35-aa94-829626f5f98e" USING gist (_full_text);


--
-- Name: 5f2e896d17824d826b32f116ea9b36a4b8028742; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "5f2e896d17824d826b32f116ea9b36a4b8028742" ON public."5238a3cd-a295-4ba1-8c93-66632195f349" USING gist (_full_text);


--
-- Name: 652fa86eb42de45a6a8d1ba7f02d26c6ab951b39; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "652fa86eb42de45a6a8d1ba7f02d26c6ab951b39" ON public."a231f002-e113-45af-9a6a-6f7d6d2b09c7" USING gist (_full_text);


--
-- Name: 67e3a490e54b1bee147d58b00317bc199b62a073; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "67e3a490e54b1bee147d58b00317bc199b62a073" ON public."baa9b849-070f-42b4-ab61-cdde26a0ab68" USING gist (to_tsvector('english'::regconfig, "Instansi"));


--
-- Name: 6f455e6f008bd5592b9e754a53613fdb35a7880f; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "6f455e6f008bd5592b9e754a53613fdb35a7880f" ON public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" USING gist (to_tsvector('english'::regconfig, "Luas Panen (Ha)"));


--
-- Name: 7518073c28ef4e0fa4f18e6a09723613a4adefe2; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "7518073c28ef4e0fa4f18e6a09723613a4adefe2" ON public."ec1f9576-dc94-4f88-bdc0-b79639b508b9" USING gist (_full_text);


--
-- Name: 75ae5db93377816f37c703618dcaf08a9ed82d2d; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "75ae5db93377816f37c703618dcaf08a9ed82d2d" ON public."6a63f1b2-c070-4d70-99dd-d9199fb15962" USING gist (to_tsvector('english'::regconfig, "Judul"));


--
-- Name: 7938324afd5b19fd488d1b406f559db8e2ac4a83; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "7938324afd5b19fd488d1b406f559db8e2ac4a83" ON public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" USING gist (to_tsvector('english'::regconfig, "RETRIBUSI"));


--
-- Name: 7f0dc75034f49a8aa2c8bd9502f3385361a757d3; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "7f0dc75034f49a8aa2c8bd9502f3385361a757d3" ON public."c9d86f58-fb73-4982-8268-c76c2c967853" USING gist (to_tsvector('english'::regconfig, "Kelurahan/Desa"));


--
-- Name: 7f3121a1a69f459dca65cdbd6830abce802d7118; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "7f3121a1a69f459dca65cdbd6830abce802d7118" ON public."284b5526-82fa-4213-bdbe-92e0d6909910" USING gist (to_tsvector('english'::regconfig, "OPD"));


--
-- Name: 7f519052648a1f30585b64672ba848e78c8a3fa9; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "7f519052648a1f30585b64672ba848e78c8a3fa9" ON public."012f05fe-7c10-4e53-85f7-5022636e571b" USING gist (to_tsvector('english'::regconfig, "Kelurahan/Desa"));


--
-- Name: 7f756d5dc1587648e2ded73f0dc99e4e39bc0a20; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "7f756d5dc1587648e2ded73f0dc99e4e39bc0a20" ON public."c9d86f58-fb73-4982-8268-c76c2c967853" USING gist (_full_text);


--
-- Name: 87152baa45135675f68040a8dfb2641fdccf7a45; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "87152baa45135675f68040a8dfb2641fdccf7a45" ON public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" USING gist (_full_text);


--
-- Name: 87b4ba61fd71f9c5a81295d661e02634f90bb590; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "87b4ba61fd71f9c5a81295d661e02634f90bb590" ON public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" USING gist (_full_text);


--
-- Name: 92cc88d6049176a1080cf20be5c36ed42037119d; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "92cc88d6049176a1080cf20be5c36ed42037119d" ON public."2a59fc35-d9ce-4543-94b1-921e30af80e8" USING gist (_full_text);


--
-- Name: 935343b55818086e6b1a9049db56241cda8dd6cb; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "935343b55818086e6b1a9049db56241cda8dd6cb" ON public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" USING gist (to_tsvector('english'::regconfig, "Judul"));


--
-- Name: 95a6b8c32efdc5870391b96398ec3d1514dbaf4e; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "95a6b8c32efdc5870391b96398ec3d1514dbaf4e" ON public."c9d86f58-fb73-4982-8268-c76c2c967853" USING gist (to_tsvector('english'::regconfig, "Persentase terhadap Luas"));


--
-- Name: 96704f6765912482d9c69296c19be65a9895803a; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "96704f6765912482d9c69296c19be65a9895803a" ON public."6a63f1b2-c070-4d70-99dd-d9199fb15962" USING gist (to_tsvector('english'::regconfig, "OPD"));


--
-- Name: 9962ad0c8261212e0972e67888b6c2baf0d4cfff; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "9962ad0c8261212e0972e67888b6c2baf0d4cfff" ON public."677400e2-f613-437d-9957-7dbd2ebb8668" USING gist (_full_text);


--
-- Name: 9a57a9b067a1a5562736940e65acca438674a396; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "9a57a9b067a1a5562736940e65acca438674a396" ON public."012f05fe-7c10-4e53-85f7-5022636e571b" USING gist (to_tsvector('english'::regconfig, "Latitude"));


--
-- Name: 9ca2e5d41b5bccb937be7ff6bc2b4f25c9720cff; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX "9ca2e5d41b5bccb937be7ff6bc2b4f25c9720cff" ON public."677400e2-f613-437d-9957-7dbd2ebb8668" USING gist (to_tsvector('english'::regconfig, "No."));


--
-- Name: a5faade359577373f93f67380718f66468e5d772; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX a5faade359577373f93f67380718f66468e5d772 ON public."012f05fe-7c10-4e53-85f7-5022636e571b" USING gist (to_tsvector('english'::regconfig, "Longitude"));


--
-- Name: accfaa8f621e4956cb228e3e7c07f2e1ff2baadc; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX accfaa8f621e4956cb228e3e7c07f2e1ff2baadc ON public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" USING gist (_full_text);


--
-- Name: ad7a437a0167f36ca1dccc96d0bfbff1244050d6; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX ad7a437a0167f36ca1dccc96d0bfbff1244050d6 ON public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" USING gist (to_tsvector('english'::regconfig, "Instansi"));


--
-- Name: b1bd1a8ab16bc7dd2238e0f74b0032a3c2774b81; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX b1bd1a8ab16bc7dd2238e0f74b0032a3c2774b81 ON public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" USING gist (_full_text);


--
-- Name: b44f2753309cd5b0d351c912501e59d6c6a33737; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX b44f2753309cd5b0d351c912501e59d6c6a33737 ON public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" USING gist (_full_text);


--
-- Name: b66afda6340a4b17c9175e716ae66746da4cf6ba; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX b66afda6340a4b17c9175e716ae66746da4cf6ba ON public."34b7eed5-191f-4459-9276-e0479b71b056" USING gist (to_tsvector('english'::regconfig, "OPD"));


--
-- Name: b7991835aaa801605911f07a0a1425cd308cb432; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX b7991835aaa801605911f07a0a1425cd308cb432 ON public."012f05fe-7c10-4e53-85f7-5022636e571b" USING gist (to_tsvector('english'::regconfig, "Ketinggian"));


--
-- Name: bb19e66f92aa72cf5b222707f43c6b13640711ed; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX bb19e66f92aa72cf5b222707f43c6b13640711ed ON public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59" USING gist (to_tsvector('english'::regconfig, "Nilai Eksport Berdasarkan Negara Tujuan"));


--
-- Name: bceda873774d38682269ae0d04532b4ffea16cee; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX bceda873774d38682269ae0d04532b4ffea16cee ON public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" USING gist (to_tsvector('english'::regconfig, "OPD"));


--
-- Name: c070cbc13fa255f621369ffac683f9b29ac285b0; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX c070cbc13fa255f621369ffac683f9b29ac285b0 ON public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" USING gist (to_tsvector('english'::regconfig, "JUMLAH"));


--
-- Name: c7181531d0e35132699dcb25fce93a9ed5fdc146; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX c7181531d0e35132699dcb25fce93a9ed5fdc146 ON public."83bb245e-17ff-4072-9520-c266964f34af" USING gist (_full_text);


--
-- Name: c886525785e0ef65ce4368ef19ad5df72d9bafde; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX c886525785e0ef65ce4368ef19ad5df72d9bafde ON public."677400e2-f613-437d-9957-7dbd2ebb8668" USING gist (to_tsvector('english'::regconfig, "Satuan"));


--
-- Name: cc94c6aa0996dd521f3be9a9918ddd5a4d71ee22; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX cc94c6aa0996dd521f3be9a9918ddd5a4d71ee22 ON public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" USING gist (to_tsvector('english'::regconfig, "Hindu"));


--
-- Name: ced2445e20e3d06d9a33c849ad4d2c817df1df37; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX ced2445e20e3d06d9a33c849ad4d2c817df1df37 ON public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" USING gist (to_tsvector('english'::regconfig, "2016"));


--
-- Name: cf22f9c8d126076fd24acfc750cea24000389a75; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX cf22f9c8d126076fd24acfc750cea24000389a75 ON public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" USING gist (to_tsvector('english'::regconfig, "No"));


--
-- Name: d22bae4b03e5502f096593dc11bfb1000fa99b26; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX d22bae4b03e5502f096593dc11bfb1000fa99b26 ON public."c9d86f58-fb73-4982-8268-c76c2c967853" USING gist (to_tsvector('english'::regconfig, "Luas (km2)"));


--
-- Name: d92f2fd67caf92ab87f4055cd672bae093b12c8d; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX d92f2fd67caf92ab87f4055cd672bae093b12c8d ON public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" USING gist (to_tsvector('english'::regconfig, "No"));


--
-- Name: d93f46f677c2fb0dc75da8ba676932bd05b721e7; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX d93f46f677c2fb0dc75da8ba676932bd05b721e7 ON public."ec1f9576-dc94-4f88-bdc0-b79639b508b9" USING gist (to_tsvector('english'::regconfig, "KECAMATAN"));


--
-- Name: dd14bdaf88f8a52200e263dd0a72210475658104; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX dd14bdaf88f8a52200e263dd0a72210475658104 ON public."749530b7-97b3-47e5-a215-03b2ad0848bc" USING gist (to_tsvector('english'::regconfig, "Judul"));


--
-- Name: e36f570015b7687cab5cc057f7a7582e13cdf02c; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX e36f570015b7687cab5cc057f7a7582e13cdf02c ON public."baa9b849-070f-42b4-ab61-cdde26a0ab68" USING gist (to_tsvector('english'::regconfig, "Konghucu"));


--
-- Name: e9c707687b3d4091d690745ad29b025706d44684; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX e9c707687b3d4091d690745ad29b025706d44684 ON public."749530b7-97b3-47e5-a215-03b2ad0848bc" USING gist (to_tsvector('english'::regconfig, "OPD"));


--
-- Name: eee5fdbdeb3ab0ccc896becd5dd572f445776e64; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX eee5fdbdeb3ab0ccc896becd5dd572f445776e64 ON public."2a59fc35-d9ce-4543-94b1-921e30af80e8" USING gist (to_tsvector('english'::regconfig, "Harapan Hidup (%)"));


--
-- Name: ef1b5d633e4224403575f5476e57426176138747; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX ef1b5d633e4224403575f5476e57426176138747 ON public."baa9b849-070f-42b4-ab61-cdde26a0ab68" USING gist (_full_text);


--
-- Name: f61fea54774a3d367fbede21d74f8623626e0a7e; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX f61fea54774a3d367fbede21d74f8623626e0a7e ON public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" USING gist (to_tsvector('english'::regconfig, "2017"));


--
-- Name: facba3e351fda6112e372929353aa3fdca72dcba; Type: INDEX; Schema: public; Owner: ckan_default
--

CREATE INDEX facba3e351fda6112e372929353aa3fdca72dcba ON public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" USING gist (to_tsvector('english'::regconfig, "Hasil (Kw /Ha)"));


--
-- Name: 012f05fe-7c10-4e53-85f7-5022636e571b zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."012f05fe-7c10-4e53-85f7-5022636e571b" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 0e2dd585-af1f-4098-8dbb-bc3916d3902c zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 0f6b5cd2-14f9-424c-b1c2-b55e229789e9 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 2592a627-a297-4997-89fb-203c76f83354 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."2592a627-a297-4997-89fb-203c76f83354" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 284b5526-82fa-4213-bdbe-92e0d6909910 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."284b5526-82fa-4213-bdbe-92e0d6909910" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 2a59fc35-d9ce-4543-94b1-921e30af80e8 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."2a59fc35-d9ce-4543-94b1-921e30af80e8" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 34b7eed5-191f-4459-9276-e0479b71b056 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."34b7eed5-191f-4459-9276-e0479b71b056" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 44e7c9ac-76e7-48d2-ae92-766bd4cbfd59 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 4a71e1cf-05bd-4ddc-9f58-defe9cb984ea zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 5238a3cd-a295-4ba1-8c93-66632195f349 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."5238a3cd-a295-4ba1-8c93-66632195f349" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 6098fa82-136c-4b35-aa94-829626f5f98e zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."6098fa82-136c-4b35-aa94-829626f5f98e" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 677400e2-f613-437d-9957-7dbd2ebb8668 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."677400e2-f613-437d-9957-7dbd2ebb8668" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 6a63f1b2-c070-4d70-99dd-d9199fb15962 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."6a63f1b2-c070-4d70-99dd-d9199fb15962" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 749530b7-97b3-47e5-a215-03b2ad0848bc zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."749530b7-97b3-47e5-a215-03b2ad0848bc" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 83bb245e-17ff-4072-9520-c266964f34af zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."83bb245e-17ff-4072-9520-c266964f34af" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 8c35e110-c18d-4ed0-adcd-5e5ee1e7debf zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: 969f5bf6-82ea-48af-8972-7de5f7aab7ca zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: a231f002-e113-45af-9a6a-6f7d6d2b09c7 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."a231f002-e113-45af-9a6a-6f7d6d2b09c7" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: baa9b849-070f-42b4-ab61-cdde26a0ab68 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."baa9b849-070f-42b4-ab61-cdde26a0ab68" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: c9d86f58-fb73-4982-8268-c76c2c967853 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."c9d86f58-fb73-4982-8268-c76c2c967853" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: ec1f9576-dc94-4f88-bdc0-b79639b508b9 zfulltext; Type: TRIGGER; Schema: public; Owner: ckan_default
--

CREATE TRIGGER zfulltext BEFORE INSERT OR UPDATE ON public."ec1f9576-dc94-4f88-bdc0-b79639b508b9" FOR EACH ROW EXECUTE PROCEDURE public.populate_full_text_trigger();


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO ckan_default;
GRANT USAGE ON SCHEMA public TO datastore_default;


--
-- Name: TABLE "012f05fe-7c10-4e53-85f7-5022636e571b"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."012f05fe-7c10-4e53-85f7-5022636e571b" TO datastore_default;


--
-- Name: TABLE "0e2dd585-af1f-4098-8dbb-bc3916d3902c"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."0e2dd585-af1f-4098-8dbb-bc3916d3902c" TO datastore_default;


--
-- Name: TABLE "0f6b5cd2-14f9-424c-b1c2-b55e229789e9"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."0f6b5cd2-14f9-424c-b1c2-b55e229789e9" TO datastore_default;


--
-- Name: TABLE "2592a627-a297-4997-89fb-203c76f83354"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."2592a627-a297-4997-89fb-203c76f83354" TO datastore_default;


--
-- Name: TABLE "284b5526-82fa-4213-bdbe-92e0d6909910"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."284b5526-82fa-4213-bdbe-92e0d6909910" TO datastore_default;


--
-- Name: TABLE "2a59fc35-d9ce-4543-94b1-921e30af80e8"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."2a59fc35-d9ce-4543-94b1-921e30af80e8" TO datastore_default;


--
-- Name: TABLE "34b7eed5-191f-4459-9276-e0479b71b056"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."34b7eed5-191f-4459-9276-e0479b71b056" TO datastore_default;


--
-- Name: TABLE "44e7c9ac-76e7-48d2-ae92-766bd4cbfd59"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."44e7c9ac-76e7-48d2-ae92-766bd4cbfd59" TO datastore_default;


--
-- Name: TABLE "4a71e1cf-05bd-4ddc-9f58-defe9cb984ea"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."4a71e1cf-05bd-4ddc-9f58-defe9cb984ea" TO datastore_default;


--
-- Name: TABLE "5238a3cd-a295-4ba1-8c93-66632195f349"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."5238a3cd-a295-4ba1-8c93-66632195f349" TO datastore_default;


--
-- Name: TABLE "6098fa82-136c-4b35-aa94-829626f5f98e"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."6098fa82-136c-4b35-aa94-829626f5f98e" TO datastore_default;


--
-- Name: TABLE "677400e2-f613-437d-9957-7dbd2ebb8668"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."677400e2-f613-437d-9957-7dbd2ebb8668" TO datastore_default;


--
-- Name: TABLE "6a63f1b2-c070-4d70-99dd-d9199fb15962"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."6a63f1b2-c070-4d70-99dd-d9199fb15962" TO datastore_default;


--
-- Name: TABLE "749530b7-97b3-47e5-a215-03b2ad0848bc"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."749530b7-97b3-47e5-a215-03b2ad0848bc" TO datastore_default;


--
-- Name: TABLE "83bb245e-17ff-4072-9520-c266964f34af"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."83bb245e-17ff-4072-9520-c266964f34af" TO datastore_default;


--
-- Name: TABLE "8c35e110-c18d-4ed0-adcd-5e5ee1e7debf"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."8c35e110-c18d-4ed0-adcd-5e5ee1e7debf" TO datastore_default;


--
-- Name: TABLE "969f5bf6-82ea-48af-8972-7de5f7aab7ca"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."969f5bf6-82ea-48af-8972-7de5f7aab7ca" TO datastore_default;


--
-- Name: TABLE _table_metadata; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public._table_metadata TO datastore_default;


--
-- Name: TABLE "a231f002-e113-45af-9a6a-6f7d6d2b09c7"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."a231f002-e113-45af-9a6a-6f7d6d2b09c7" TO datastore_default;


--
-- Name: TABLE "b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."b3e7b9eb-fbe9-49b2-97b8-2401fc9a3851" TO datastore_default;


--
-- Name: TABLE "baa9b849-070f-42b4-ab61-cdde26a0ab68"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."baa9b849-070f-42b4-ab61-cdde26a0ab68" TO datastore_default;


--
-- Name: TABLE "bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."bccc4ca9-91e1-4c2e-bf2a-b69fc7d39171" TO datastore_default;


--
-- Name: TABLE "c9d86f58-fb73-4982-8268-c76c2c967853"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."c9d86f58-fb73-4982-8268-c76c2c967853" TO datastore_default;


--
-- Name: TABLE "cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."cc0623d9-0f38-4f4d-bcff-3a25b2ada6fb" TO datastore_default;


--
-- Name: TABLE "ec1f9576-dc94-4f88-bdc0-b79639b508b9"; Type: ACL; Schema: public; Owner: ckan_default
--

GRANT SELECT ON TABLE public."ec1f9576-dc94-4f88-bdc0-b79639b508b9" TO datastore_default;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: ckan_default
--

ALTER DEFAULT PRIVILEGES FOR ROLE ckan_default IN SCHEMA public REVOKE ALL ON TABLES  FROM ckan_default;
ALTER DEFAULT PRIVILEGES FOR ROLE ckan_default IN SCHEMA public GRANT SELECT ON TABLES  TO datastore_default;


--
-- PostgreSQL database dump complete
--

